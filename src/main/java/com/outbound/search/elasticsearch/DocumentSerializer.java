package com.outbound.search.elasticsearch;

import java.util.HashMap;
import java.util.Map;

import com.outbound.search.company.Company;
import com.outbound.search.employee.Employee;
import com.outbound.search.industry.Industry;
import com.outbound.search.jobtitle.JobTitle;
import com.outbound.search.location.Location;
import com.outbound.search.person.Person;
import static com.outbound.search.elasticsearch.Mappings.Companies;
import static com.outbound.search.elasticsearch.Mappings.Industries;
import static com.outbound.search.elasticsearch.Mappings.Contacts;
import static com.outbound.search.elasticsearch.Mappings.JobTitles;
import static com.outbound.search.elasticsearch.Mappings.Locations;

// Serializes models so that they can be indexed into elasticsearch.
public class DocumentSerializer {
  public static Map<String, Object> serializeCompany(Company company, Location location, Industry industry) {
    Map<String, Object> serializedCompany = new HashMap<>();

    serializedCompany.put(Companies.ID_FIELD, company.getId());
    serializedCompany.put(Companies.NAME_FIELD, company.getName());
    serializedCompany.put(Companies.WEBSITE_URL, company.getWebsiteUrl());
    serializedCompany.put(Companies.LOGO_URL, company.getLogoUrl());
    serializedCompany.put(Companies.DESCRIPTION_FIELD, company.getDescription());
    serializedCompany.put(Companies.PHONE_FIELD, company.getPhone());
    serializedCompany.put(Companies.EMPLOYEE_SIZE_FIELD, company.getEmployeeSize());
    serializedCompany.put(Companies.INDUSTRY_ID_FIELD, company.getIndustryId());

    if (location != null) {
      serializedCompany.put(Companies.LOCATION_FIELD, serializeLocation(location));
    }

    if (industry != null) {
      serializedCompany.put(Companies.INDUSTRY_FIELD, industry.getDisplayName());
    }

    removeEmptyValues(serializedCompany);

    return serializedCompany;
  }


  public static Map<String, Object> serializeIndustry(Industry industry) {
    Map<String, Object> serializedIndustry = new HashMap<>();

    serializedIndustry.put(Industries.ID_FIELD, industry.getId());
    serializedIndustry.put(Industries.INDUSTRY_FIELD, industry.getIndustry());
    serializedIndustry.put(Industries.DISPLAY_NAME_FIELD, industry.getDisplayName());

    removeEmptyValues(serializedIndustry);

    return serializedIndustry;
  }

  public static Map<String, Object> serializeContact(
      Employee employee, Person person, Location location, JobTitle jobTitle, Company company
    ) {
    Map<String, Object> serializedContact = new HashMap<>();
    serializedContact.put(Contacts.EMPLOYEE_ID_FIELD, employee.getId());
    serializedContact.put(Contacts.CONTACT_ID_FIELD, employee.getContactId());
    serializedContact.put(Contacts.COMPANY_ID_FIELD, employee.getCompanyId());
    serializedContact.put(Contacts.JOB_TITLE_ID_FIELD, employee.getJobTitleId());
    serializedContact.put(Contacts.SENIORITY_FIELD, employee.getSeniority());
    serializedContact.put(Contacts.FUNCTION_FIELD, employee.getFunction());
    serializedContact.put(Contacts.STATUS_FIELD, employee.getStatus());

    if (person != null) {
      serializedContact.put(Contacts.NAME_FIELD, person.getFullName());
      serializedContact.put(Contacts.EMAIL_FIELD, person.getEmail());
      serializedContact.put(Contacts.LINKEDIN_PHOTO, person.getLinkedinPhoto());
      serializedContact.put(Contacts.DIRECT_PHONE_FIELD, person.getDirectPhone());
      serializedContact.put(Contacts.CELL_PHONE_FIELD, person.getCellPhone());
    }

    if (location != null) {
      serializedContact.put(Contacts.LOCATION_FIELD, serializeLocation(location));
    }

    if (jobTitle != null) {
      serializedContact.put(Contacts.JOB_TITLE_FIELD, jobTitle.getDisplayName());
    }

    if (company != null) {
      serializedContact.put(Contacts.COMPANY_FIELD, company.getName());
    }

    removeEmptyValues(serializedContact);

    return serializedContact;
  }

  public static Map<String, Object> serializeJobTitle(JobTitle jobTitle) {
    Map<String, Object> serializedJobTitle = new HashMap<>();

    serializedJobTitle.put(JobTitles.ID_FIELD, jobTitle.getId());
    serializedJobTitle.put(JobTitles.JOB_TITLE_FIELD, jobTitle.getJobTitle());
    serializedJobTitle.put(JobTitles.DISPLAY_NAME_FIELD, jobTitle.getDisplayName());

    removeEmptyValues(serializedJobTitle);

    return serializedJobTitle;
  }

  public static Map<String, Object> serializeLocation(Location location) {
    Map<String, Object> serializedLocation = new HashMap<>();

    serializedLocation.put(Locations.ID_FIELD, location.getId());
    serializedLocation.put(Locations.CITY_FIELD, location.getCity());
    serializedLocation.put(Locations.STATE_FIELD, location.getState());
    serializedLocation.put(Locations.COUNTRY_FIELD, location.getCountry());
    serializedLocation.put(Locations.POSTAL_FIELD, location.getPostal());
    serializedLocation.put(Locations.ADDRESS_FIELD, location.getAddress());

    removeEmptyValues(serializedLocation);

    return serializedLocation;
  }

  private static void removeEmptyValues(Map<String, Object> serializedData) {
    serializedData.values().removeIf(v -> v == null || v.toString().trim().length() == 0);
  }
}
