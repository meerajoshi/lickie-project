package com.outbound.search;

import java.io.IOException;

import com.outbound.search.elasticsearch.QueryExecutor;
import com.outbound.search.elasticsearch.querybuilders.CompaniesQueryBuilder;
import com.outbound.search.elasticsearch.querybuilders.ContactsQueryBuilder;
import com.outbound.search.queries.CompaniesQuery;
import com.outbound.search.queries.CompaniesQueryHit;
import com.outbound.search.queries.ContactsQuery;
import com.outbound.search.queries.ContactsQueryHit;
import com.outbound.search.queries.QueryResponse;

public class SearchUtil {

	public static QueryResponse<ContactsQueryHit> doContactsSearch(ContactsQuery query) throws IOException {
		CompaniesQuery companiesQuery = query.getCompaniesQuery();

		if(companiesQuery.getCompanyName() != null || companiesQuery.getIndustryIds() != null || companiesQuery.getStates() != null || companiesQuery.getMaxEmployeeSize() != null || companiesQuery.getMinEmployeeSize() != null) {
			CompaniesQueryBuilder companiesQueryBuilder = new CompaniesQueryBuilder(companiesQuery);
			QueryResponse<CompaniesQueryHit> companyHits = new QueryExecutor<>(companiesQueryBuilder).execute();

			int[] companyIds = new int[companyHits.getHits().size()];

			for (int idx = 0; idx < companyIds.length; idx++) {
				companyIds[idx] = companyHits.getHits().get(idx).getId();
			}

			query.setCompanyIds(companyIds);
		}

		ContactsQueryBuilder queryBuilder = new ContactsQueryBuilder(query);
		return new QueryExecutor<>(queryBuilder).execute();
	}

	public static QueryResponse<CompaniesQueryHit> doCompaniesSearch(CompaniesQuery query) throws IOException {
		CompaniesQueryBuilder queryBuilder = new CompaniesQueryBuilder(query);
		return new QueryExecutor<>(queryBuilder).execute();
	}

}
