package com.outbound.application.rest.model;

import com.outbound.search.employee.Employee;
import com.outbound.search.employee.EmployeeFactory;
import com.outbound.search.jobtitle.JobTitle;
import com.outbound.search.jobtitle.JobTitleFactory;

public class EmploymentDetailsUtil {

	public static EmploymentDetails getCurrentEmploymentDetailsForContact(final int personId) {
		Employee currentEmployeeRecord = EmployeeFactory.getEmployeeByPersonId(personId);
		JobTitle currentJobTitle = JobTitleFactory.getJobTitle(currentEmployeeRecord.getJobTitleId());
		CompanyDetails currentCompanyDetails = CompanyDetailsUtil.getCompanyDetails(currentEmployeeRecord.getCompanyId());

		return new EmploymentDetails(currentEmployeeRecord, currentJobTitle, currentCompanyDetails);
	}

}
