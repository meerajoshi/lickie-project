package com.outbound.search.queries;

public class ContactsQueryHit {
  private int id;
  private int employeeId;
  private String name;
  private String email;
  private String hrFunction;
  private String seniority;
  private String jobTitle;
  private String state;
  private String linkedinPhoto;
  private String directPhone;
  private String cellPhone;
  private String company;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getLinkedinPhoto() {
    return linkedinPhoto;
  }

  public void setLinkedinPhoto(String linkedinPhoto) {
    this.linkedinPhoto = linkedinPhoto;
  }

  public String getCellPhone() {
    return cellPhone;
  }

  public void setCellPhone(String cellPhone) {
    this.cellPhone = cellPhone;
  }

  public String getDirectPhone() {
    return directPhone;
  }

  public void setDirectPhone(String directPhone) {
    this.directPhone = directPhone;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getSeniority() {
    return seniority;
  }

  public void setSeniority(String seniority) {
    this.seniority = seniority;
  }

  public String getHrFunction() {
    return hrFunction;
  }

  public void setHrFunction(String hrFunction) {
    this.hrFunction = hrFunction;
  }

  public void setEmployeeId(int employeeId) {
    this.employeeId = employeeId;
  }

  public int getEmployeeId() {
    return this.employeeId;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "ContactsQueryHit(employeeId=" + this.employeeId + ", email=" + this.email + ", name=" + this.name + ")";
  }
}
