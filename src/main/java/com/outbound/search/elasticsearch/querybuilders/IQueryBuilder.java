package com.outbound.search.elasticsearch.querybuilders;

import org.elasticsearch.index.query.QueryBuilder;

import org.elasticsearch.search.SearchHit;

public interface IQueryBuilder<T> {
  String[] getSelectedFields();
  int getOffset();
  int getLimit();
  String getIndexName();
  QueryBuilder buildRootQuery();
  T parseSearchHit(SearchHit searchHit);
}
