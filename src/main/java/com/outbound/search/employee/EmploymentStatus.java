package com.outbound.search.employee;

public enum EmploymentStatus {
	CURRENT("Current"), PREVIOUS("Previous");

	private String label;

	EmploymentStatus(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
