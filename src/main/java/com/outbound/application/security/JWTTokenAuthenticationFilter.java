package com.outbound.application.security;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.outbound.slack.SlackExceptionMessageBuilder;
import com.outbound.slack.SlackWebhooks;
import com.outbound.user.User;
import com.outbound.user.UserFactory;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class JWTTokenAuthenticationFilter extends OncePerRequestFilter {

    private final JWTConfig jwtConfig;

    public JWTTokenAuthenticationFilter(JWTConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        // 1. get the authentication header. Tokens are supposed to be passed in the authentication header
        String header = request.getHeader(jwtConfig.getHeader());

        // 2. validate the header and check the prefix
        if(header == null || !header.startsWith(jwtConfig.getPrefix())) {
            chain.doFilter(request, response);  		// If not valid, go to the next filter.
            return;
        }

        // If there is no token provided and hence the user won't be authenticated.
        // It's Ok. Maybe the user accessing a public path or asking for a token.

        // All secured paths that needs a token are already defined and secured in config class.
        // And If user tried to access without access token, then he won't be authenticated and an exception will be thrown.

        // 3. Get the token
        String token = header.replace(jwtConfig.getPrefix(), "");

        try {	// exceptions might be thrown in creating the claims if for example the token is expired

            // 4. Validate the token
            authenticateToken(token, jwtConfig);
        } catch (Exception e) {
            // In case of failure. Make sure it's clear; so guarantee user won't be authenticated
            SecurityContextHolder.clearContext();
        }

        // go to the next filter in the filter chain
        chain.doFilter(request, response);
    }

    public static void authenticateToken(String token, JWTConfig jwtConfig) throws SQLException {
        // 6. Authenticate the user
        // Now, user is authenticated
        SecurityContextHolder.getContext().setAuthentication(getAuthenticationTokenFromJWT(token, jwtConfig));
    }

    public static UsernamePasswordAuthenticationToken getAuthenticationTokenFromJWT(String token, JWTConfig jwtConfig) throws SQLException {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtConfig.getSecret().getBytes())
                .parseClaimsJws(token)
                .getBody();

        String username = claims.getSubject();

        if(username != null) {
            @SuppressWarnings("unchecked")
            List<String> authorities = (List<String>) claims.get("authorities");

            // 5. Create auth object
            // UsernamePasswordAuthenticationToken: A built-in object, used by spring to represent the current authenticated / being authenticated user.
            // It needs a list of authorities, which has type of GrantedAuthority interface, where SimpleGrantedAuthority is an implementation of that interface
            UserFactory userFactory = new UserFactory();
            User user = null;

            try {
                userFactory.init();
                user = userFactory.getUser(username);
            } catch (SQLException e) {
                try {
                    SlackExceptionMessageBuilder exceptionMessageBuilder = new SlackExceptionMessageBuilder();
                    exceptionMessageBuilder.addException(e)
                            .addTextBlock("Failed to get authorized user by username: " + username)
                            .build().sendMessage(SlackWebhooks.INCOMING_WEBHOOK_VIRTUAL_CLASSROOM);
                } catch (IOException ioException) {
                    //TODO log this
                }

                throw e; //Make sure we fail out because we don't want to authenticate usernames that don't exist!
            }

            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    user, null, authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

            return auth;
        }

        return null;
    }
}
