package com.outbound.search.industry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Statement;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class IndustryFactory extends DBFactory {

  public static Industry getIndustry(Integer id) {
    Industry industry = null;

    final String query = "select * from search_industry where search_industry_id = ?";
    try (Connection connection = DBConnector.getConnection(); PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setInt(1, id);

      ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        industry = initIndustryFromRS(rs);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return industry;
  }

  public List<Industry> getAllIndustries() {
    List<Industry> industries = new ArrayList<>();

    try (Connection connection = DBConnector.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("select * from search_industry");

      ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        industries.add(initIndustryFromRS(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return industries;
  }

  public static Industry getIndustryByName(String name) {
    Industry industry = null;

    try (Connection connection = DBConnector.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("select * from search_industry where industry = ?");
      statement.setString(1, name.toLowerCase());

      ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        industry = initIndustryFromRS(rs);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return industry;
  }

  public static Integer insertIndustry(Industry industry) {
    Integer id = null;

    try (Connection connection = DBConnector.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("insert into search_industry (industry, display_name) values (?, ?)",
          Statement.RETURN_GENERATED_KEYS);

      statement.setString(1, industry.getIndustry().toLowerCase());
      statement.setString(2, industry.getDisplayName());

      statement.execute();
      ResultSet rs = statement.getGeneratedKeys();
      if (rs.next()) {
        id = rs.getInt(1);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return id;
  }

  public static Integer insertOrGetExistingIndustry(Industry industry) {
    Industry existingIndustry = getIndustryByName(industry.getIndustry());
    if (existingIndustry != null) {
      return existingIndustry.getId();
    }

    return insertIndustry(industry);
  }

  private static Industry initIndustryFromRS(ResultSet rs) throws SQLException {
    Industry industry = new Industry();

    industry.setId(rs.getInt("search_industry_id"));
    industry.setIndustry(rs.getString("industry"));
    industry.setDisplayName(rs.getString("display_name"));

    return industry;
  }
}
