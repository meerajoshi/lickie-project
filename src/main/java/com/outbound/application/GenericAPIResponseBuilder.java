package com.outbound.application;

public class GenericAPIResponseBuilder extends APIResponseBuilder {

	private Object data;

	@Override
	public APIResponse build() {
		APIResponse response = new APIResponse();

		response.setMessage(this.getMessage());
		response.setSuccess(this.isSuccess());

		if(this.data != null) {
			response.setData(this.data);
		}

		return response;
	}

	public APIResponseBuilder withData(Object data) {
		this.data = data;
		return this;
	}
}
