package com.outbound.search.location;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class LocationFactory extends DBFactory {

	public static Location getLocation(Integer id) {
		Location location = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_location where search_location_id = ?");
			statement.setInt(1, id);

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				location = initLocationFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return location;
	}

	/**
	 * WARNING: Only use this for indexing purposes. Not to be used for searching!
	 * @return A list containing all {@link Location} records in the database.
	 */
	public List<Location> getAllLocations() {
		List<Location> locationList = new ArrayList<>();

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_location");

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				locationList.add(initLocationFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return locationList;
	}

	public List<Location> getLocations(List<Integer> ids) {
		List<Location> locationList = new ArrayList<>();
		String queryString = String.join(", ", Collections.nCopies(ids.size(), "?"));

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(new StringBuilder("select * from search_location where search_location_id in (").append(queryString).append(")").toString());

			for(int idx=1; idx<=ids.size(); idx++) {
				statement.setInt(idx, ids.get(idx-1));
			}

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				locationList.add(initLocationFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return locationList;
	}

	public static Integer insertLocation(Location location) {
		Integer id = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("insert into search_location (city, state, postal, country, address) values (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, location.getCity());
			statement.setString(2, location.getState());
			statement.setString(3, location.getPostal());
			statement.setString(4, location.getCountry());
			statement.setString(5, location.getAddress());

			statement.execute();
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return id;
	}

	private static Location initLocationFromRS(ResultSet rs) throws SQLException {
		Location location = new Location();

		location.setId(rs.getInt("search_location_id"));
		location.setCity(rs.getString("city"));
		location.setState(rs.getString("state"));
		location.setPostal(rs.getString("postal"));
		location.setCountry(rs.getString("country"));
		location.setAddress(rs.getString("address"));

		return location;
	}

}
