package com.email.sendgrid;

import java.io.IOException;
import java.sql.SQLException;

import com.outbound.user.User;
import com.outbound.user.UserInvite;
import com.outbound.user.UserResetPasswordRequestFactory;
import com.outbound.util.EnvironmentUtil;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

import static com.email.sendgrid.SendGridConstants.TRAINING_VIEW_SUPPORT_EMAIL;

public class SendGridUtil {

	public static boolean sendPasswordResetEmail(User user) throws SQLException {
		boolean success = false;

		//Start by inserting a request password record
		UserResetPasswordRequestFactory passwordRequestFactory = new UserResetPasswordRequestFactory();
		passwordRequestFactory.init();

		String hash = passwordRequestFactory.insertNewUserResetPasswordRequest(user);
		String resetPasswordLink = EnvironmentUtil.getAppUrl() + "/app/account/reset-password?u=" + hash + "&i="+user.getId();

		Email from = new Email(TRAINING_VIEW_SUPPORT_EMAIL);
		from.setName("TrainingView Support");
		String subject = "TrainingView | Reset Password";
		Email to = new Email(user.getEmail());
		Content content = new Content("text/html", "<p>Hello " + user.getFirstName() + ",</p><p>Please reset your password by using the following link: <a href=\""+resetPasswordLink+"\">Reset Password</a></p><p>If you did not request to reset your password, please ignore this email.</p>");
		Mail mail = new Mail(from, subject, to, content);

		SendGrid sg = new SendGrid(SendGridConstants.TRAINING_VIEW_API_KEY);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			success = response.getStatusCode() == 202;
		} catch (IOException ex) {
			ex.printStackTrace();//TODO log
		}

		return success;
	}

	public static boolean sendUserInviteEmail(User sentBy, UserInvite invite, String invitee) throws SQLException {
		boolean success = false;

		String setupAccountLink = EnvironmentUtil.getAppUrl() + "/app/account/setup-account?u=" + invite.getHash() + "&i="+invite.getUserId();

		Email from = new Email(TRAINING_VIEW_SUPPORT_EMAIL);
		from.setName("TrainingView Support");
		String subject = "TrainingView | Invite";
		Email to = new Email(invitee);
		Content content = new Content("text/html", "<p>You have been invited by "+sentBy.getFirstName() + " " + sentBy.getLastName() + " to create an account.</p><p>Please use the following link to setup your account: <a href=\"" + setupAccountLink + "\">Setup Account</a></p>");
		Mail mail = new Mail(from, subject, to, content);

		SendGrid sg = new SendGrid(SendGridConstants.TRAINING_VIEW_API_KEY);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			success = response.getStatusCode() == 202;
		} catch (IOException ex) {
			ex.printStackTrace();//TODO log
		}

		return success;
	}

}
