package com.outbound.search.elasticsearch;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.outbound.application.configuration.ElasticsearchClientConfig;
import com.outbound.search.elasticsearch.querybuilders.IQueryBuilder;
import com.outbound.search.queries.QueryResponse;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

public class QueryExecutor<T> {
  private IQueryBuilder<T> queryBuilder;
  private RestHighLevelClient elasticsearchClient;

  public QueryExecutor(IQueryBuilder<T> queryBuilder) {
    this.queryBuilder = queryBuilder;
  }

  public QueryResponse<T> execute() throws IOException {
    SearchSourceBuilder sourceBuilder = new SearchSourceBuilder()
      .query(this.queryBuilder.buildRootQuery())
      .fetchSource(this.queryBuilder.getSelectedFields(), new String[] {})
      .from(this.queryBuilder.getOffset())
      .size(this.queryBuilder.getLimit());

    SearchRequest searchRequest = new SearchRequest()
      .indices(this.queryBuilder.getIndexName())
      .source(sourceBuilder);

    SearchResponse apiResponse = getElasticsearchClient().search(searchRequest, RequestOptions.DEFAULT);

    SearchHits apiHits = apiResponse.getHits();

    List<T> hits = Arrays.stream(apiHits.getHits())
      .map(this.queryBuilder::parseSearchHit)
      .collect(Collectors.toList());

    return new QueryResponse<>(apiHits.getTotalHits().value, this.queryBuilder.getOffset(), hits);
  }

   // TODO: This should be configured via dependency injection.
   private RestHighLevelClient getElasticsearchClient() {
    if (this.elasticsearchClient != null) {
      return this.elasticsearchClient;
    }

    this.elasticsearchClient = new ElasticsearchClientConfig().elasticsearchClient();
    return this.elasticsearchClient;
  }
}
