package com.outbound.search.industry;

import com.outbound.model.DataModel;

public class Industry extends DataModel {

  private int id;

  /**
   * Normalized, lowercased industry name, used for de-duplicating data.
   */
  private String industry;

  /**
   * Non-normalized industry name to be displayed to the user.
   */
  private String displayName;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

    public String getIndustry() {
    return industry;
  }

  public void setIndustry(String industry) {
    // Always save value as lower case strings to normalize the data
    this.industry = industry.toLowerCase();
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}
