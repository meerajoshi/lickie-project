package com.outbound.search.queries;

// A query definition to perform a simple autocomplete query
// against a single field.
public class SimpleAutocompleteQuery {
  private String query;
  private int[] idsToExclude;
  private int offset;
  private int limit;

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  public int[] getIdsToExclude() {
    return idsToExclude;
  }

  public void setIdsToExclude(int[] idsToExclude) {
    this.idsToExclude = idsToExclude;
  }
}
