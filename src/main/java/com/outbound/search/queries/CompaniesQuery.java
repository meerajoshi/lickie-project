package com.outbound.search.queries;

public class CompaniesQuery {
  private String companyName;
  private int[] industryIds;
  private Integer minEmployeeSize;
  private Integer maxEmployeeSize;
  private String[] states;
  private int offset;
  private int limit;

  public String getCompanyName() {
    return companyName;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String[] getStates() {
    return states;
  }

  public void setState(String[] states) {
    this.states = states;
  }

  public Integer getMaxEmployeeSize() {
    return maxEmployeeSize;
  }

  public void setMaxEmployeeSize(Integer maxEmployeeSize) {
    this.maxEmployeeSize = maxEmployeeSize;
  }

  public Integer getMinEmployeeSize() {
    return minEmployeeSize;
  }

  public void setMinEmployeeSize(Integer minEmployeeSize) {
    this.minEmployeeSize = minEmployeeSize;
  }

  public int[] getIndustryIds() {
    return industryIds;
  }

  public void setIndustryIds(int[] industryIds) {
    this.industryIds = industryIds;
  }
}
