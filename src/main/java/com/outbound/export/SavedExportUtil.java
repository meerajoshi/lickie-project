package com.outbound.export;

import com.google.gson.Gson;
import com.outbound.application.rest.model.ExportRequest;
import com.outbound.search.queries.CompaniesQuery;
import com.outbound.search.queries.ContactsQuery;

public class SavedExportUtil {

	public static SavedExport buildSavedExportFromExportRequest(ExportRequest exportRequest, ExportType exportType, int userId) {
		SavedExport savedExport = new SavedExport();
		savedExport.setName(exportRequest.getName());
		savedExport.setDescription(exportRequest.getDescription());
		savedExport.setIds(exportRequest.getIdList());
		savedExport.setType(exportType);
		savedExport.setCreatedBy(userId);

		Gson gson = new Gson();

		if(exportType.equals(ExportType.COMPANY)) {
			if(exportRequest.getCompaniesQuery() != null) {
				savedExport.setOriginalSearchQuery(gson.toJson(exportRequest.getCompaniesQuery()));
			}
		} else if(exportType.equals(ExportType.CONTACT)) {
			if(exportRequest.getContactsQuery() != null) {
				savedExport.setOriginalSearchQuery(gson.toJson(exportRequest.getContactsQuery()));
			}
		}

		return savedExport;
	}

	public static ExportRequest buildExportRequest(SavedExport savedExport, boolean shouldSave) {
		ExportRequest exportRequest = new ExportRequest();
		exportRequest.setShouldSave(shouldSave);
		exportRequest.setName(savedExport.getName());
		exportRequest.setDescription(savedExport.getDescription());

		if(savedExport.getOriginalSearchQuery() != null && savedExport.getOriginalSearchQuery().trim().length() > 0) {
			Gson gson = new Gson();

			if (savedExport.getType().equals(ExportType.COMPANY)) {
				exportRequest.setCompaniesQuery(gson.fromJson(savedExport.getOriginalSearchQuery(), CompaniesQuery.class));
			} else if (savedExport.getType().equals(ExportType.CONTACT)) {
				exportRequest.setContactsQuery(gson.fromJson(savedExport.getOriginalSearchQuery(), ContactsQuery.class));
			}
		} else {
			exportRequest.setIdList(savedExport.getIds());
		}

		return exportRequest;
	}

	public static ContactsQuery getContactsQueryFromSavedExport(SavedExport savedExport) {
		Gson gson = new Gson();
		return gson.fromJson(savedExport.getOriginalSearchQuery(), ContactsQuery.class);
	}

	public static CompaniesQuery getCompaniesQueryFromSavedExport(SavedExport savedExport) {
		Gson gson = new Gson();
		return gson.fromJson(savedExport.getOriginalSearchQuery(), CompaniesQuery.class);
	}

}
