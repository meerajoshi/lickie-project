package com.outbound.user;

public enum UserRole {
	SUPER_USER("Super User", 0), INTERNAL_ADMIN("Internal Admin", 1), INTERNAL("Internal", 2), ADMIN("Admin", 3), BASIC("Basic", 4);

	private Integer roleId;
	private String label;
	private int roleLevel;

	UserRole(String label, int roleLevel) {
		this.label = label;
		this.roleLevel = roleLevel;
	}

	public Integer getRoleId() {
		return roleId;
	}

	void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getLabel() {
		return label;
	}

	void setLabel(String label) {
		this.label = label;
	}

	public int getRoleLevel() {
		return roleLevel;
	}

	void setRoleLevel(int roleLevel) {
		this.roleLevel = roleLevel;
	}
}
