package com.outbound.application.rest.lists;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import com.outbound.application.APIResponse;
import com.outbound.application.GenericAPIResponseBuilder;
import com.outbound.export.SavedExport;
import com.outbound.export.SavedExportFactory;
import com.outbound.user.User;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static com.outbound.application.rest.util.RestDatesUtil.getLocalDateTimeString;

@RestController
@RequestMapping("/lists")
@PreAuthorize("isAuthenticated()")
public class Lists {

	@GetMapping("/get")
	@ResponseBody
	public APIResponse getSavedExports(UsernamePasswordAuthenticationToken token) {
		final User user = (User) token.getPrincipal();

		try {
			return new GenericAPIResponseBuilder().withData(getAllUserSavedExports(user)).withSuccess(true).build();
		} catch (SQLException throwables) {
			return new GenericAPIResponseBuilder().withSuccess(false).withMessage(throwables.getMessage()).build();
		}
	}

	@PostMapping("/delete")
	@ResponseBody
	public APIResponse deleteSavedExport(UsernamePasswordAuthenticationToken token, @RequestParam int savedExportId) {
		final User user = (User) token.getPrincipal();

		try {
			SavedExportFactory.deleteSavedExport(savedExportId);
			return new GenericAPIResponseBuilder().withData(getAllUserSavedExports(user)).withSuccess(true).build();
		} catch (SQLException throwables) {
			return new GenericAPIResponseBuilder().withSuccess(false).withMessage(throwables.getMessage()).build();
		}
	}

	@PostMapping("/rename")
	@ResponseBody
	public APIResponse renameSavedExport(UsernamePasswordAuthenticationToken token, @RequestParam int savedExportId, @RequestParam String name) {
		final User user = (User) token.getPrincipal();

		try {
			SavedExportFactory.renameSavedExport(savedExportId, name);
			return new GenericAPIResponseBuilder().withData(getAllUserSavedExports(user)).withSuccess(true).build();
		} catch (SQLException throwables) {
			return new GenericAPIResponseBuilder().withSuccess(false).withMessage(throwables.getMessage()).build();
		}
	}

	private List<SavedExportLite> getAllUserSavedExports(User user) throws SQLException {
		List<SavedExport> savedExports = SavedExportFactory.getUserExports(user.getId());
		return savedExports.stream().map(SavedExportLite::new).collect(Collectors.toList());
	}

	static class SavedExportLite {
		private int id;
		private String name;
		private String description;
		private String createdOn;
		private String modifiedOn;
		private int resultSize;
		private String type;

		public SavedExportLite(SavedExport savedExport) {
			this.id = savedExport.getId();
			this.name = savedExport.getName();
			this.description = savedExport.getDescription();
			this.type = savedExport.getType().name();
			this.resultSize = savedExport.getIds().size();
			this.createdOn = getLocalDateTimeString(savedExport.getCreatedOn());
			this.modifiedOn = getLocalDateTimeString(savedExport.getModifiedOn());
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}

		public int getResultSize() {
			return resultSize;
		}

		public void setResultSize(int resultSize) {
			this.resultSize = resultSize;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}
}
