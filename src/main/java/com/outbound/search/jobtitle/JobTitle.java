package com.outbound.search.jobtitle;

import com.outbound.model.DataModel;

public class JobTitle extends DataModel {
  private int id;

  /**
   * Normalized, lowercased job title, used for de-duplicating data.
   */
  private String jobTitle;

  /**
   * Non-normalized job title to be displayed to the user
   */
  private String displayName;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    // Always save value as lower case strings to normalize the data
    this.jobTitle = jobTitle.toLowerCase();
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}