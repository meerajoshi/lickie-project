package com.outbound.application.rest.model;

import com.outbound.search.employee.Employee;
import com.outbound.search.jobtitle.JobTitle;

public class EmploymentDetails {

	private final Employee employee;
	private final JobTitle jobTitle;
	private final CompanyDetails companyDetails;

	public EmploymentDetails(Employee employee, JobTitle jobTitle, CompanyDetails companyDetails) {
		this.employee = employee;
		this.jobTitle = jobTitle;
		this.companyDetails = companyDetails;
	}

	public Employee getEmployee() {
		return employee;
	}

	public JobTitle getJobTitle() {
		return jobTitle;
	}

	public CompanyDetails getCompanyDetails() {
		return companyDetails;
	}
}
