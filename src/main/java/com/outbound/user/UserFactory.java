package com.outbound.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class UserFactory extends DBFactory {

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();

        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from user");

            ResultSet rs = statement.executeQuery();

            while(rs.next()) {
                users.add(initUserFromRS(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO log this
        }

        return users;
    }

    public List<User> getUsersByCompany(int companyId) throws SQLException {
        List<User> companyUsers = new ArrayList<>();

        try(Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from user where company_id = ?");
            statement.setInt(1, companyId);

            statement.executeQuery();
            ResultSet rs = statement.getResultSet();

            while(rs.next()) {
                companyUsers.add(initUserFromRS(rs));
            }
        }

        return companyUsers;
    }

    public User getUser(String username) throws SQLException {
        User user = null;

        try (Connection connection = DBConnector.getConnection()) {

            PreparedStatement statement = connection.prepareStatement("select * from user as u where u.username = ?");
            statement.setString(1, username);

            ResultSet rs = statement.executeQuery();

            if(rs.next()) {
                user = initUserFromRS(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    public User getUserByEmail(String email) throws SQLException {
        User user = null;

        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from user as u where u.email = ?");
            statement.setString(1, email);

            ResultSet rs = statement.executeQuery();

            if(rs.next()) {
                user = initUserFromRS(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    public User getUserById(Integer userId) throws SQLException {
        User user = null;

        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from user as u where u.user_id = ?");
            statement.setInt(1, userId);

            ResultSet rs = statement.executeQuery();

            if(rs.next()) {
                user = initUserFromRS(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    public void deleteUser(int userId) {
        try(Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("delete from user where user_id = ?");
            statement.setInt(1, userId);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO log
        }
    }

    public String getPasswordHash(String username) throws SQLException {
        String hash = null;

        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select u.password_hash as hash from user as u where u.username = ?");
            statement.setString(1, username);

            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                hash = rs.getString("hash");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return hash;
    }

    public boolean isEmailUnique(String email) throws SQLException {
        return getUserByEmail(email) == null;
    }

    public boolean isUsernameUnique(String username) throws SQLException {
        boolean isUnique = false;

        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select count(u.user_id) as hash from user as u where u.username = ?");
            statement.setString(1, username);

            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                isUnique = rs.getInt(1) <= 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return isUnique;
    }

    public void updateUserPassword(User user, String newPasswordHash) throws SQLException {
        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("update user set password_hash = ? where user_id = ?");
            statement.setString(1, newPasswordHash);
            statement.setInt(2, user.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO log
        }
    }

    public void updateUser(User user) {
        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("update user set password_hash = ?, username = ?, user_role_id = ?, first_name = ?, last_name = ? where user_id = ?");
            int paramIdx = 1;
            statement.setString(paramIdx++, user.getPasswordHash());
            statement.setString(paramIdx++, user.getUsername());
            statement.setInt(paramIdx++, user.getUserRoleId());
            statement.setString(paramIdx++, user.getFirstName());
            statement.setString(paramIdx++, user.getLastName());
            statement.setInt(paramIdx++, user.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO log
        }
    }

    public Integer insertUser(User user) throws SQLException {
        Integer userId = null;

        try (Connection connection = DBConnector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("insert into user (username, password_hash, email, first_name, last_name, user_role_id, company_id) values (?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

            int paramIdx = 1;
            statement.setString(paramIdx++, user.getUsername());
            statement.setString(paramIdx++, user.getPasswordHash());
            statement.setString(paramIdx++, user.getEmail());
            statement.setString(paramIdx++, user.getFirstName());
            statement.setString(paramIdx++, user.getLastName());
            statement.setInt(paramIdx++, user.getUserRoleId());
            statement.setInt(paramIdx++, user.getCompanyId());

            statement.execute();
            ResultSet rs = statement.getGeneratedKeys();
            if(rs.next()){
                userId = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userId;
    }

    public static User initUserFromRS(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("user_id"));
        if(rs.getInt("company_id") != 0) {
            user.setCompanyId(rs.getInt("company_id"));
        }
        user.setUserRoleId(rs.getInt("user_role_id"));
        user.setUsername(rs.getString("username"));
        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setEmail(rs.getString("email"));

        return user;
    }

}
