package com.outbound.application.configuration;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

@Configuration
public class ElasticsearchClientConfig extends AbstractElasticsearchConfiguration {

  @Override
  @Bean
  public RestHighLevelClient elasticsearchClient() {
    final String elasticHost = System.getenv("elastic_host");
	//final String elasticHost ="localhost:9200";
    final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
            .connectedTo(elasticHost)
            .build();

    return RestClients.create(clientConfiguration).rest();
  }
}
