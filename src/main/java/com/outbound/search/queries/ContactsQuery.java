package com.outbound.search.queries;

// DTO defining how to query the contacts data
public class ContactsQuery {
  private String nameOrEmail; // Search on both name and email
  private String[] hrFunction;
  private String[] seniority;
  private int[] jobTitles;
  private String[] states;
  private int[] companyIds;
  private String[] requiredContactFields;
  private String requiredContactsFieldOp; // "any" or "all"
  private String companyName;
  private int[] industryIds;
  private Integer minEmployeeSize;
  private Integer maxEmployeeSize;

  private int offset;
  private int limit;

  public CompaniesQuery getCompaniesQuery() {
    CompaniesQuery companiesQuery = new CompaniesQuery();

    companiesQuery.setCompanyName(this.companyName);
    companiesQuery.setIndustryIds(this.industryIds);
    companiesQuery.setMinEmployeeSize(this.minEmployeeSize);
    companiesQuery.setMaxEmployeeSize(this.maxEmployeeSize);
    companiesQuery.setState(this.states);
    companiesQuery.setOffset(0);
    companiesQuery.setLimit(10000);//TODO make this not hardcoded or something without a limit

    return companiesQuery;
  }

  public int[] getCompanyIds() {
    return companyIds;
  }

  public void setCompanyIds(int[] companyIds) {
    this.companyIds = companyIds;
  }

  public String[] getStates() {
    return states;
  }

  public void setStates(String[] states) {
    this.states = states;
  }

  public int[] getJobTitles() {
    return jobTitles;
  }

  public void setJobTitles(int[] jobTitles) {
    this.jobTitles = jobTitles;
  }

  public String getRequiredContactsFieldOp() {
    return requiredContactsFieldOp;
  }

  public void setRequiredContactsFieldOp(String requiredContactsFieldOp) {
    this.requiredContactsFieldOp = requiredContactsFieldOp;
  }

  public String[] getRequiredContactFields() {
    return requiredContactFields;
  }

  public void setRequiredContactFields(String[] requiredContactFields) {
    this.requiredContactFields = requiredContactFields;
  }

  public String getNameOrEmail() {
    return nameOrEmail;
  }

  public void setNameOrEmail(String nameOrEmail) {
    this.nameOrEmail = nameOrEmail;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public String[] getSeniority() {
    return seniority;
  }

  public void setSeniority(String[] seniority) {
    this.seniority = seniority;
  }

  public String[] getHrFunction() {
    return hrFunction;
  }

  public void setHrFunction(String[] hrFunction) {
    this.hrFunction = hrFunction;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public int[] getIndustryIds() {
    return industryIds;
  }

  public void setIndustryIds(int[] industryIds) {
    this.industryIds = industryIds;
  }

  public Integer getMinEmployeeSize() {
    return minEmployeeSize;
  }

  public void setMinEmployeeSize(Integer minEmployeeSize) {
    this.minEmployeeSize = minEmployeeSize;
  }

  public Integer getMaxEmployeeSize() {
    return maxEmployeeSize;
  }

  public void setMaxEmployeeSize(Integer maxEmployeeSize) {
    this.maxEmployeeSize = maxEmployeeSize;
  }
}
