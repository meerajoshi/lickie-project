package com.outbound.application.rest.auth;

import java.io.Serializable;

import com.outbound.user.User;
import com.outbound.user.UserRole;

public class UserModel implements Serializable {

	private Integer id;
	private Integer companyId;
	private String username;
	private String email;
	private String firstName;
	private String lastName;
	private UserRole role;
	private Integer roleLevel;

	public UserModel(User user, UserRole role) {
		this.id = user.getId();
		this.companyId = user.getCompanyId();
		this.username = user.getUsername();
		this.email = user.getEmail();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.role = role;
		this.roleLevel = role.getRoleLevel();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
		this.roleLevel = role.getRoleLevel();
	}

	public Integer getRoleLevel() {
		return roleLevel;
	}
}
