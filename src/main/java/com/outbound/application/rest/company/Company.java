package com.outbound.application.rest.company;

import com.outbound.application.APIResponse;
import com.outbound.application.GenericAPIResponseBuilder;
import com.outbound.application.rest.model.CompanyDetails;
import com.outbound.application.rest.model.CompanyDetailsUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/companies")
@PreAuthorize("isAuthenticated()")
public class Company {

	@GetMapping("/get")
	@ResponseBody
	public APIResponse getCompany(@RequestParam(name = "company") Integer id) {
		CompanyDetails companyDetails = CompanyDetailsUtil.getCompanyDetails(id);
		return new GenericAPIResponseBuilder().withData(companyDetails).build();
	}
}
