package com.outbound.search.jobtitle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;
import java.sql.PreparedStatement;

public class JobTitleFactory extends DBFactory {

  public static JobTitle getJobTitle(Integer id) {
    JobTitle title = null;

    final String query = "select * from search_job_title where search_job_title_id = ?";
    try (Connection connection = DBConnector.getConnection(); PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setInt(1, id);

      ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        title = initJobTitleFromRS(rs);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return title;
  }

  public List<JobTitle> getAllJobTitles() {
    List<JobTitle> jobTitles = new ArrayList<>();

    try (Connection connection = DBConnector.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("select * from search_job_title");

      ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        jobTitles.add(initJobTitleFromRS(rs));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return jobTitles;
  }

  public JobTitle getJobTitleByName(String name) {
    JobTitle jobTitle = null;

    try (Connection connection = DBConnector.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("select * from search_job_title where job_title = ?");
      statement.setString(1, name.toLowerCase());

      ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        jobTitle = initJobTitleFromRS(rs);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return jobTitle;
  }

  public Integer insertJobTitle(JobTitle jobTitle) {
    Integer id = null;

    try (Connection connection = DBConnector.getConnection()) {
      PreparedStatement statement = connection.prepareStatement("insert into search_job_title (job_title, display_name) values (?, ?)",
          Statement.RETURN_GENERATED_KEYS);

      statement.setString(1, jobTitle.getJobTitle().toLowerCase());
      statement.setString(2, jobTitle.getDisplayName());

      statement.execute();
      ResultSet rs = statement.getGeneratedKeys();
      if (rs.next()) {
        id = rs.getInt(1);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      // TODO log
    }

    return id;
  }

  public Integer insertOrGetExistingJobTitle(JobTitle jobTitle) {
    JobTitle existingJobTitle = getJobTitleByName(jobTitle.getJobTitle());
    if (existingJobTitle != null) {
      return existingJobTitle.getId();
    }

    return insertJobTitle(jobTitle);
  }

  private static JobTitle initJobTitleFromRS(ResultSet rs) throws SQLException {
    JobTitle title = new JobTitle();

    title.setId(rs.getInt("search_job_title_id"));
    title.setJobTitle(rs.getString("job_title"));
    title.setDisplayName(rs.getString("display_name"));

    return title;
  }
}
