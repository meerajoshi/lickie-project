package com.outbound.application.rest.model;

import com.outbound.search.location.Location;
import com.outbound.search.person.Person;

public class ContactDetails {

	private final Person person;
	private final Location location;
	private final EmploymentDetails currentEmploymentDetails;

	public ContactDetails(Person person, Location location, EmploymentDetails currentEmploymentDetails) {
		this.person = person;
		this.location = location;
		this.currentEmploymentDetails = currentEmploymentDetails;
	}

	public Person getPerson() {
		return person;
	}

	public Location getLocation() {
		return location;
	}

	public EmploymentDetails getCurrentEmploymentDetails() {
		return currentEmploymentDetails;
	}
}
