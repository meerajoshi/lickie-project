package com.outbound.export;

import java.util.List;

import com.outbound.model.TrackedDataModel;

public class SavedExport extends TrackedDataModel {

	private Integer id;
	private String name;
	private String description;
	private ExportType type;
	private List<Integer> ids;
	private String originalSearchQuery;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ExportType getType() {
		return type;
	}

	public void setType(ExportType type) {
		this.type = type;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public String getOriginalSearchQuery() {
		return originalSearchQuery;
	}

	public void setOriginalSearchQuery(String originalSearchQuery) {
		this.originalSearchQuery = originalSearchQuery;
	}
}
