package com.outbound.slack;

import java.io.PrintWriter;
import java.io.StringWriter;

public class SlackExceptionMessageBuilder extends SlackMessageBuilder {

    public SlackExceptionMessageBuilder addException(Exception e) {
        //Add Exception Information
        StringWriter sw = new StringWriter();
        PrintWriter stackTrace = new PrintWriter(sw, true);
        e.printStackTrace(stackTrace);

        addStringAttachment("*Stack Trace:*```" + sw.getBuffer().toString() + "```");
        return this;
    }
}
