package com.outbound.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TrackedDataModel extends DataModel {

	private Integer createdBy;

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public static void initTrackedDataModelFromRS(ResultSet rs, TrackedDataModel model) throws SQLException {
		model.setCreatedBy(rs.getInt("created_by"));
		initDataModelFromRS(rs, model);
	}
}
