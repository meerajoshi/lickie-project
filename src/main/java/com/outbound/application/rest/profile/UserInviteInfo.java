package com.outbound.application.rest.profile;

import com.outbound.user.User;
import com.outbound.user.UserInvite;

public class UserInviteInfo {

	private Integer id;
	private Integer userId;
	private String userEmail;
	private boolean completed;

	public UserInviteInfo(UserInvite invite, User user) {
		this.id = invite.getId();
		this.userId = invite.getUserId();
		this.userEmail = user.getEmail();
		this.completed = invite.isCompleted();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
}
