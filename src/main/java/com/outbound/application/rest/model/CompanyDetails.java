package com.outbound.application.rest.model;

import com.outbound.search.company.Company;
import com.outbound.search.industry.Industry;
import com.outbound.search.location.Location;

public class CompanyDetails {

	private final Company company;
	private final Industry industry;
	private final Location location;

	public CompanyDetails(Company company, Industry industry, Location location) {
		this.company = company;
		this.industry = industry;
		this.location = location;
	}

	public Company getCompany() {
		return company;
	}

	public Industry getIndustry() {
		return industry;
	}

	public Location getLocation() {
		return location;
	}
}
