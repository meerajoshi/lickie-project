package com.outbound.application.rest.model;

import com.outbound.search.company.Company;
import com.outbound.search.company.CompanyFactory;
import com.outbound.search.industry.Industry;
import com.outbound.search.industry.IndustryFactory;
import com.outbound.search.location.Location;
import com.outbound.search.location.LocationFactory;

public class CompanyDetailsUtil {

	public static CompanyDetails getCompanyDetails(final int companyId) {
		Company currentCompany = CompanyFactory.getCompany(companyId);
		Industry industry = IndustryFactory.getIndustry(currentCompany.getIndustryId());
		Location companyLocation = LocationFactory.getLocation(currentCompany.getLocationId());

		return new CompanyDetails(currentCompany, industry, companyLocation);
	}

}
