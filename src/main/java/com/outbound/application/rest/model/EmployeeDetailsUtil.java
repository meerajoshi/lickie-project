package com.outbound.application.rest.model;

import java.util.List;

import com.outbound.search.employee_search.EmployeeSearchFactory;
import com.outbound.search.employee_search.EmployeeSearch;


public class EmployeeDetailsUtil {
	public static EmployeeDetails getCurrentCompanyEmployee(final int companyId) {
		List<EmployeeSearch> currentEmployeeRecord = EmployeeSearchFactory.getEmployeesByCompanyId(companyId);
		return new EmployeeDetails(currentEmployeeRecord);
	}
}
