package com.outbound.application;

import java.io.Serializable;

public class APIResponse implements Serializable {

    private Object data;
    private String message;
    private boolean success = true;

    public APIResponse() {
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
