package com.outbound.export;

import java.util.Arrays;
import java.util.List;

import com.outbound.application.rest.model.ExportRequest;
import com.outbound.search.queries.CompaniesQuery;
import com.outbound.search.queries.ContactsQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SavedExportUtilTest {

	private static final String exportName = "test";
	private static final int userId = 1;
	private static final List<Integer> idList = Arrays.asList(1, 2, 3, 4);
	private static final String companyName = "company";
	private static final String nameEmail = "name";
	private CompaniesQuery companyQuery;
	private ContactsQuery contactQuery;
	private ExportRequest exportRequest;

	@BeforeEach
	void setUp() {
		exportRequest = new ExportRequest();
		exportRequest.setIdList(idList);
		exportRequest.setName(exportName);

		companyQuery = new CompaniesQuery();
		companyQuery.setCompanyName(companyName);
		exportRequest.setCompaniesQuery(companyQuery);

		contactQuery = new ContactsQuery();
		contactQuery.setNameOrEmail(nameEmail);
		exportRequest.setContactsQuery(contactQuery);
	}

	@Test
	void buildSavedExportFromExportRequestCompany() {
		SavedExport export = SavedExportUtil.buildSavedExportFromExportRequest(exportRequest, ExportType.COMPANY, userId);

		assertEquals(exportName, export.getName());
		assertEquals(idList, export.getIds());
		assertEquals(userId, export.getCreatedBy());
		assertEquals(ExportType.COMPANY, export.getType());
		assertEquals(companyName, SavedExportUtil.getCompaniesQueryFromSavedExport(export).getCompanyName());
	}

	@Test
	void buildSavedExportFromExportRequestContact() {
		SavedExport export = SavedExportUtil.buildSavedExportFromExportRequest(exportRequest, ExportType.CONTACT, userId);

		assertEquals(exportName, export.getName());
		assertEquals(idList, export.getIds());
		assertEquals(userId, export.getCreatedBy());
		assertEquals(ExportType.CONTACT, export.getType());
		assertEquals(nameEmail, SavedExportUtil.getContactsQueryFromSavedExport(export).getNameOrEmail());
	}
}