package com.outbound.search.company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class CompanyFactory extends DBFactory {

	public static Company getCompany(Integer id) {
		Company company = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_company where search_company_id = ?");
			statement.setInt(1, id);

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				company = initCompanyFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return company;
	}

	public static Company getCompanyByName(String name) {
		Company company = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_company where name = ?");
			statement.setString(1, name);

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				company = initCompanyFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return company;
	}

	public static Integer insertCompany(Company company) {
		Integer id = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("insert into search_company (name, search_location_id, description, phone, employee_size, search_industry_id, linkedin_profile, logo_url, website_url) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, company.getName());
			statement.setInt(2, company.getLocationId());
			statement.setString(3, company.getDescription());
			statement.setString(4, company.getPhone());
			statement.setInt(5, company.getEmployeeSize());
			statement.setInt(6, company.getIndustryId());
			statement.setString(7, company.getLinkedinProfile());
			statement.setString(8, company.getLogoUrl());
			statement.setString(9, company.getWebsiteUrl());


			statement.execute();
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return id;
	}

	/**
	 * WARNING: Only use this for indexing purposes. Not to be used for searching!
	 * @return A list containing all {@link Company} records in the database.
	 */
	public List<Company> getAllCompanies() {
		List<Company> companyList = new ArrayList<>();

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_company");

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				companyList.add(initCompanyFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return companyList;
	}

	public List<Company> getCompanies(List<Integer> ids) {
		List<Company> companyList = new ArrayList<>();
		String queryString = String.join(", ", Collections.nCopies(ids.size(), "?"));

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(new StringBuilder("select * from search_company where search_company_id in (").append(queryString).append(")").toString());

			for(int idx=1; idx<=ids.size(); idx++) {
				statement.setInt(idx, ids.get(idx-1));
			}

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				companyList.add(initCompanyFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return companyList;
	}

	private static Company initCompanyFromRS(ResultSet rs) throws SQLException {
		Company company = new Company();

		company.setId(rs.getInt("search_company_id"));
		company.setName(rs.getString("name"));
		company.setLocationId(rs.getInt("search_location_id"));
		company.setDescription(rs.getString("description"));
		company.setPhone(rs.getString("phone"));
		company.setEmployeeSize(rs.getInt("employee_size"));
		company.setIndustryId(rs.getInt("search_industry_id"));
		company.setLinkedinProfile(rs.getString("linkedin_profile"));
		company.setLogoUrl(rs.getString("logo_url"));
		company.setWebsiteUrl(rs.getString("website_url"));

		return company;
	}

}
