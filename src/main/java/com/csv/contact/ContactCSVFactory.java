package com.csv.contact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.search.employee.EmploymentStatus;

public class ContactCSVFactory {

	public static List<ContactCSVEntry> getContactCSVEntries(List<Integer> contactIds) throws SQLException {
		List<ContactCSVEntry> entries = new ArrayList<>();

		//<editor-fold Query>
		StringBuilder query = new StringBuilder();
		query.append("select ");
		query.append(" distinct(person.search_person_id) as person_id,");
		query.append(" person.first_name as first_name,");
		query.append(" person.last_name as last_name,");
		query.append(" person.email as email,");
		query.append(" person.direct_phone as direct_phone,");
		query.append(" person.cell_phone as cell_phone,");
		query.append(" person.linkedin_profile as linkedin_profile,");
		query.append(" employee.seniority as seniority,");
		query.append(" employee.function as function,");
		query.append(" jobTitle.display_name as job_title,");
		query.append(" personLocation.address as contact_address,");
		query.append(" personLocation.city as contact_city,");
		query.append(" personLocation.state as contact_state,");
		query.append(" personLocation.postal as contact_postal,");
		query.append(" personLocation.country as contact_country,");
		query.append(" company.name as company_name,");
		query.append(" company.description as company_description,");
		query.append(" company.phone as company_phone,");
		query.append(" company.employee_size as company_employee_size,");
		query.append(" industry.display_name as company_industry,");
		query.append(" company.linkedin_profile as company_linkedin_profile,");
		query.append(" company.website_url as company_website_url,");
		query.append(" companyLocation.address as company_address,");
		query.append(" companyLocation.city as company_city,");
		query.append(" companyLocation.state as company_state,");
		query.append(" companyLocation.postal as company_postal,");
		query.append(" companyLocation.country as company_country");
		query.append(" from search_person person");
		query.append(" inner join search_employee employee on person.search_person_id = employee.search_contact_id");
		query.append(" inner join search_company company on employee.search_company_id = company.search_company_id");
		query.append(" inner join search_industry industry on company.search_industry_id = industry.search_industry_id");
		query.append(" inner join search_job_title jobTitle on employee.search_job_title_id = jobTitle.search_job_title_id");
		query.append(" inner join search_location companyLocation on company.search_location_id = companyLocation.search_location_id");
		query.append(" inner join search_location personLocation on person.search_location_id = personLocation.search_location_id");
		query.append(" where ");
		query.append(" employee.status = ?");
		query.append(" and person.search_person_id in (");

		for(int i=0; i<contactIds.size(); i++) {
			query.append(" ?");

			if(i+1 < contactIds.size()) {
				query.append(",");
			}
		}

		query.append(")");
		//</editor-fold>

		try (Connection c = DBConnector.getConnection(); PreparedStatement statement = c.prepareStatement(query.toString())) {
			int stmtIdx = 1;

			statement.setString(stmtIdx++, EmploymentStatus.CURRENT.name());

			for(Integer contactId : contactIds) {
				statement.setInt(stmtIdx++, contactId);
			}

			try (ResultSet rs = statement.executeQuery()) {
				while(rs.next()) {
					entries.add(initFromRS(rs));
				}
			}
		}

		return entries;
	}

	public static List<ContactCSVEntry> getCompanyContactCSVEntries(List<Integer> companyIds) throws SQLException {
		List<ContactCSVEntry> entries = new ArrayList<>();

		//<editor-fold Query>
		StringBuilder query = new StringBuilder();
		query.append("select ");
		query.append(" person.first_name as first_name,");
		query.append(" person.last_name as last_name,");
		query.append(" person.email as email,");
		query.append(" person.direct_phone as direct_phone,");
		query.append(" person.cell_phone as cell_phone,");
		query.append(" person.linkedin_profile as linkedin_profile,");
		query.append(" employee.seniority as seniority,");
		query.append(" employee.function as function,");
		query.append(" jobTitle.display_name as job_title,");
		query.append(" personLocation.address as contact_address,");
		query.append(" personLocation.city as contact_city,");
		query.append(" personLocation.state as contact_state,");
		query.append(" personLocation.postal as contact_postal,");
		query.append(" personLocation.country as contact_country,");
		query.append(" company.name as company_name,");
		query.append(" company.description as company_description,");
		query.append(" company.phone as company_phone,");
		query.append(" company.employee_size as company_employee_size,");
		query.append(" industry.display_name as company_industry,");
		query.append(" company.linkedin_profile as company_linkedin_profile,");
		query.append(" company.website_url as company_website_url,");
		query.append(" companyLocation.address as company_address,");
		query.append(" companyLocation.city as company_city,");
		query.append(" companyLocation.state as company_state,");
		query.append(" companyLocation.postal as company_postal,");
		query.append(" companyLocation.country as company_country");
		query.append(" from search_person person");
		query.append(" inner join search_employee employee on person.search_person_id = employee.search_contact_id");
		query.append(" inner join search_company company on employee.search_company_id = company.search_company_id");
		query.append(" inner join search_industry industry on company.search_industry_id = industry.search_industry_id");
		query.append(" inner join search_job_title jobTitle on employee.search_job_title_id = jobTitle.search_job_title_id");
		query.append(" inner join search_location companyLocation on company.search_location_id = companyLocation.search_location_id");
		query.append(" inner join search_location personLocation on person.search_location_id = personLocation.search_location_id");
		query.append(" where ");
		query.append(" employee.status = ?");
		query.append(" and company.search_company_id in (");

		for(int i=0; i<companyIds.size(); i++) {
			query.append(" ?");

			if(i+1 < companyIds.size()) {
				query.append(",");
			}
		}

		query.append(")");
		//</editor-fold>

		try (Connection c = DBConnector.getConnection(); PreparedStatement statement = c.prepareStatement(query.toString())) {
			int stmtIdx = 1;

			statement.setString(stmtIdx++, EmploymentStatus.CURRENT.name());

			for(Integer companyId : companyIds) {
				statement.setInt(stmtIdx++, companyId);
			}

			try (ResultSet rs = statement.executeQuery()) {
				while(rs.next()) {
					entries.add(initFromRS(rs));
				}
			}
		}

		return entries;
	}

	private static ContactCSVEntry initFromRS(ResultSet rs) throws SQLException {
		ContactCSVEntry contactCSVEntry = new ContactCSVEntry();

		contactCSVEntry.setFirstName(rs.getString("first_name"));
		contactCSVEntry.setLastName(rs.getString("last_name"));
		contactCSVEntry.setEmail(rs.getString("email"));
		contactCSVEntry.setDirectPhone(rs.getString("direct_phone"));
		contactCSVEntry.setCellPhone(rs.getString("cell_phone"));
		contactCSVEntry.setLinkedinProfile(rs.getString("linkedin_profile"));
		contactCSVEntry.setSeniority(rs.getString("seniority"));
		contactCSVEntry.setFunction(rs.getString("function"));
		contactCSVEntry.setJobTitle(rs.getString("job_title"));
		contactCSVEntry.setContactAddress(rs.getString("contact_address"));
		contactCSVEntry.setContactCity(rs.getString("contact_city"));
		contactCSVEntry.setContactState(rs.getString("contact_state"));
		contactCSVEntry.setContactPostal(rs.getString("contact_postal"));
		contactCSVEntry.setContactCountry(rs.getString("contact_country"));
		contactCSVEntry.setCompany(rs.getString("company_name"));
		contactCSVEntry.setCompanyDescription(rs.getString("company_description"));
		contactCSVEntry.setCompanyPhone(rs.getString("company_phone"));
		contactCSVEntry.setCompanyEmployeeSize(rs.getInt("company_employee_size"));
		contactCSVEntry.setCompanyIndustry(rs.getString("company_industry"));
		contactCSVEntry.setCompanyLinkedinProfile(rs.getString("company_linkedin_profile"));
		contactCSVEntry.setCompanyWebsiteUrl(rs.getString("company_website_url"));
		contactCSVEntry.setCompanyAddress(rs.getString("company_address"));
		contactCSVEntry.setCompanyCity(rs.getString("company_city"));
		contactCSVEntry.setCompanyState(rs.getString("company_state"));
		contactCSVEntry.setCompanyPostal(rs.getString("company_postal"));
		contactCSVEntry.setCompanyCountry(rs.getString("company_country"));

		return contactCSVEntry;
	}

}
