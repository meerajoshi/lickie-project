package com.outbound.search.elasticsearch.querybuilders;

import com.outbound.search.queries.CompaniesQuery;
import com.outbound.search.queries.CompaniesQueryHit;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import static com.outbound.search.elasticsearch.Mappings.Companies;
import static com.outbound.search.elasticsearch.Mappings.Locations;

import java.util.Map;

public class CompaniesQueryBuilder implements IQueryBuilder<CompaniesQueryHit> {
  private CompaniesQuery queryDefinition;

  public CompaniesQueryBuilder(CompaniesQuery queryDefinition) {
    this.queryDefinition = queryDefinition;
  }

  @Override
  public String[] getSelectedFields() {
    return new String[] {
      Companies.ID_FIELD,
      Companies.NAME_FIELD,
      Companies.WEBSITE_URL,
      Companies.LOGO_URL,
      Companies.INDUSTRY_FIELD,
      Companies.EMPLOYEE_SIZE_FIELD,
      Companies.LOCATION_FIELD + "." + Locations.STATE_FIELD,
      Companies.LOCATION_FIELD + "." + Locations.CITY_FIELD
    };
  }

  @Override
  public String getIndexName() {
    return Companies.INDEX_NAME;
  }

  @Override
  public int getOffset() {
    return this.queryDefinition.getOffset();
  }

  @Override
  public int getLimit() {
    return this.queryDefinition.getLimit();
  }

  @Override
  public CompaniesQueryHit parseSearchHit(SearchHit searchHit) {
    Map<String, Object> hitSource = searchHit.getSourceAsMap();
    
    CompaniesQueryHit hit = new CompaniesQueryHit();

    hit.setId((int) hitSource.get(Companies.ID_FIELD));
    hit.setName((String) hitSource.get(Companies.NAME_FIELD));
    hit.setWebsiteUrl((String) hitSource.get(Companies.WEBSITE_URL));
    hit.setLogoUrl((String) hitSource.get(Companies.LOGO_URL));
    hit.setEmployeeSize((int) hitSource.get(Companies.EMPLOYEE_SIZE_FIELD));
    hit.setIndustry((String) hitSource.get(Companies.INDUSTRY_FIELD));

    Map<String, Object> locationSource = (Map<String, Object>) hitSource.get(Companies.LOCATION_FIELD);
    if (locationSource != null) {
      hit.setState((String) locationSource.get(Locations.STATE_FIELD));
      hit.setCity((String) locationSource.get(Locations.CITY_FIELD));
    }

    return hit;
  }
  
  @Override
  public QueryBuilder buildRootQuery() {
    QueryBuilder matchName = buildNameQuery();
    QueryBuilder matchIndustryIds = buildIndustryIdsQuery();
    QueryBuilder matchEmployeeSize = buildEmployeeSizeQuery();
    QueryBuilder matchStates = buildStatesQuery();

    if (matchName == null &&
      matchIndustryIds == null &&
      matchEmployeeSize == null &&
      matchStates == null
    ) {
      return QueryBuilders.matchAllQuery();
    }

    BoolQueryBuilder rootQuery = QueryBuilders.boolQuery();

    if (matchName != null) {
      rootQuery.must(matchName);
    }

    if (matchIndustryIds != null) {
      rootQuery.filter(matchIndustryIds);
    }

    if (matchEmployeeSize != null) {
      rootQuery.filter(matchEmployeeSize);
    }

    if (matchStates != null) {
      rootQuery.filter(matchStates);
    }

    return rootQuery;
  }

  private QueryBuilder buildNameQuery() {
    String name = this.queryDefinition.getCompanyName();
    if (name == null || name.trim().length() == 0) {
      return null;
    }

    return QueryBuilderUtils.buildAutoCompleteQuery(Companies.NAME_FIELD, name);
  }

  private QueryBuilder buildIndustryIdsQuery() {
    int[] industryIds = this.queryDefinition.getIndustryIds();
    if (industryIds == null || industryIds.length == 0) {
      return null;
    }

    return QueryBuilders.termsQuery(Companies.INDUSTRY_ID_FIELD, industryIds);
  }

  private QueryBuilder buildEmployeeSizeQuery() {
    Integer maxEmployeeSize = this.queryDefinition.getMaxEmployeeSize();
    Integer minEmployeeSize = this.queryDefinition.getMinEmployeeSize();

    if (minEmployeeSize == null || maxEmployeeSize == null || (minEmployeeSize == -1 && maxEmployeeSize == -1)) {
      return null;
    }

    RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery(Companies.EMPLOYEE_SIZE_FIELD);

    if (minEmployeeSize >= 0) {
      rangeQuery.gte(minEmployeeSize);
    }

    if (maxEmployeeSize >= 0) {
      rangeQuery.lte(maxEmployeeSize);
    }

    return rangeQuery;
  }

  private QueryBuilder buildStatesQuery() {
    String[] states = this.queryDefinition.getStates();
    if (states == null || states.length == 0) {
      return null;
    }

    return QueryBuilders
      .termsQuery(Companies.LOCATION_FIELD + "." + Locations.STATE_FIELD, states);
  }
}
