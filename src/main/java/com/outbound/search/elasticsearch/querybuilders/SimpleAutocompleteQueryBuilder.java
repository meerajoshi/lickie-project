package com.outbound.search.elasticsearch.querybuilders;

import java.util.Map;

import com.outbound.search.queries.SimpleAutocompleteQuery;
import com.outbound.search.queries.SimpleAutocompleteQueryHit;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

public class SimpleAutocompleteQueryBuilder implements IQueryBuilder<SimpleAutocompleteQueryHit> {
  private SimpleAutocompleteQuery queryDefinition;
  private String indexName;
  private String idField;
  private String nameField;
  private String displayNameField;

  public SimpleAutocompleteQueryBuilder(SimpleAutocompleteQuery queryDefinition, String indexName, String idField,
      String nameField, String displayNameField) {
    this.queryDefinition = queryDefinition;
    this.indexName = indexName;
    this.idField = idField;
    this.nameField = nameField;
    this.displayNameField = displayNameField;
  }

  @Override
  public String[] getSelectedFields() {
    return new String[] { this.idField, this.displayNameField };
  }

  @Override
  public String getIndexName() {
    return this.indexName;
  }

  @Override
  public int getOffset() {
    return this.queryDefinition.getOffset();
  }

  @Override
  public int getLimit() {
    return this.queryDefinition.getLimit();
  }

  @Override
  public SimpleAutocompleteQueryHit parseSearchHit(SearchHit searchHit) {
    Map<String, Object> hitSource = searchHit.getSourceAsMap();

    int id = (int) hitSource.get(this.idField);
    String displayName = (String) hitSource.get(this.displayNameField);

    return new SimpleAutocompleteQueryHit(id, displayName);
  }

  @Override
  public QueryBuilder buildRootQuery() {
    QueryBuilder matchName = buildNameQuery();
    QueryBuilder matchExcludedIds = buildExcludeIdsQuery();

    if (matchExcludedIds == null) {
      return matchName;
    }

    return QueryBuilders.boolQuery()
      .must(matchName)
      .mustNot(matchExcludedIds);
  }

  private QueryBuilder buildNameQuery() {
    return QueryBuilderUtils.buildAutoCompleteQuery(
      this.nameField,
      this.queryDefinition.getQuery()
    );
  }

  private QueryBuilder buildExcludeIdsQuery() {
    int[] idsToExclude = this.queryDefinition.getIdsToExclude();
    if (idsToExclude == null || idsToExclude.length == 0) {
      return null;
    }

    return QueryBuilders.termsQuery(this.idField, idsToExclude);
  }
}
