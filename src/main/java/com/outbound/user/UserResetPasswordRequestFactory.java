package com.outbound.user;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;
import org.apache.commons.codec.binary.Base64;
import org.springframework.lang.Nullable;

public class UserResetPasswordRequestFactory extends DBFactory {

	public String insertNewUserResetPasswordRequest(User user) throws SQLException {
		String userResetPasswordRequestHash = null;

		try (Connection connection = DBConnector.getConnection()) {
			String hash = getRequestHash(user);

			PreparedStatement statement = connection.prepareStatement("insert into user_reset_password_request (user_id, reset_password_request_hash, completed) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, user.getId());
			statement.setString(2, hash);
			statement.setBoolean(3, false);

			statement.execute();
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()){
				userResetPasswordRequestHash = hash;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return userResetPasswordRequestHash;
	}

	@Nullable
	public UserResetPasswordRequest getUserResetPasswordRequest(String hash, Integer userId) {
		UserResetPasswordRequest passwordRequest = null;

		try(Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from user_reset_password_request where user_id = ? and reset_password_request_hash = ?");
			statement.setInt(1, userId);
			statement.setString(2, hash);

			statement.execute();
			ResultSet rs = statement.getResultSet();

			if(rs.next()){
				passwordRequest = initResetPasswordRequestFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return passwordRequest;
	}

	public void updateRequest(UserResetPasswordRequest resetPasswordRequest) {
		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("update user_reset_password_request set completed = ? where user_reset_password_request_id = ?");
			statement.setBoolean(1, resetPasswordRequest.isCompleted());
			statement.setInt(2, resetPasswordRequest.getId());
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}
	}

	private static String getRequestHash(User user) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			//TODO log
		}

		String stringToHash = user.getEmail() + user.getLastName() + LocalDateTime.now().toString();

		messageDigest.update(stringToHash.getBytes(StandardCharsets.UTF_8));
		return Base64.encodeBase64URLSafeString(messageDigest.digest());
	}

	public static UserResetPasswordRequest initResetPasswordRequestFromRS(ResultSet rs) throws SQLException {
		UserResetPasswordRequest userResetPasswordRequest = new UserResetPasswordRequest();
		userResetPasswordRequest.setId(rs.getInt("user_reset_password_request_id"));
		userResetPasswordRequest.setUserId(rs.getInt("user_id"));
		userResetPasswordRequest.setHash(rs.getString("reset_password_request_hash"));
		userResetPasswordRequest.setCompleted(rs.getBoolean("completed"));

		return userResetPasswordRequest;
	}

}
