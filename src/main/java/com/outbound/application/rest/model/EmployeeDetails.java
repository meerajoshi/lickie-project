package com.outbound.application.rest.model;

import java.util.List;

import com.outbound.search.employee_search.EmployeeSearch;

public class EmployeeDetails {
	private final List<EmployeeSearch> employee;
	

	public EmployeeDetails(List<EmployeeSearch> employee) {
		this.employee = employee;
	}

	public List<EmployeeSearch> getEmployees() {
		return employee;
	}

}
