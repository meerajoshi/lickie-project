package com.outbound.application.rest.model;

import com.outbound.search.location.Location;
import com.outbound.search.location.LocationFactory;
import com.outbound.search.person.Person;
import com.outbound.search.person.PersonFactory;

public class ContactDetailsUtil {

	public static ContactDetails getContactDetails(final int personId) {
		//Fetch our information about this contact
		Person person = PersonFactory.getPerson(personId);
		Location contactLocation = LocationFactory.getLocation(person.getLocationId());

		//Get the current employment details
		EmploymentDetails currentEmploymentDetails = EmploymentDetailsUtil.getCurrentEmploymentDetailsForContact(personId);

		return new ContactDetails(person, contactLocation, currentEmploymentDetails);
	}

}
