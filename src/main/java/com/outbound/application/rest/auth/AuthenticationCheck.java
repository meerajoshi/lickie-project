package com.outbound.application.rest.auth;

import com.outbound.application.APIResponse;
import com.outbound.user.User;
import com.outbound.user.UserRole;
import com.outbound.user.UserRoleFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authentication")
public class AuthenticationCheck {

	@GetMapping("/check")
	@ResponseBody
	public APIResponse checkAuthenticationStatus(UsernamePasswordAuthenticationToken token) {

		APIResponse response = new APIResponse();
		response.setSuccess(token.isAuthenticated());
		final User user = (User) token.getPrincipal();

		UserRoleFactory userRoleFactory = new UserRoleFactory();
		userRoleFactory.init();

		final UserRole role = userRoleFactory.getUserRole(user.getUserRoleId());
		response.setData(new UserModel(user, role));

		return response;
	}

}
