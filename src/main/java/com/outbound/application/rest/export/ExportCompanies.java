package com.outbound.application.rest.export;

import java.io.IOException;
import java.sql.SQLException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.csv.contact.ContactCSVEntry;
import com.csv.contact.ContactCSVFactory;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.outbound.application.rest.model.ExportRequest;
import com.outbound.export.ExportType;
import com.outbound.export.SavedExport;
import com.outbound.export.SavedExportFactory;
import com.outbound.export.SavedExportUtil;
import com.outbound.search.SearchUtil;
import com.outbound.search.queries.CompaniesQuery;
import com.outbound.search.queries.CompaniesQueryHit;
import com.outbound.search.queries.QueryResponse;
import com.outbound.user.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/export/companies")
@PreAuthorize("isAuthenticated()")
public class ExportCompanies {

	@PostMapping(
			consumes = {MediaType.APPLICATION_JSON_VALUE}
	)
	@ResponseBody
	public void exportCompanies(
			HttpServletResponse response,
			@RequestBody ExportRequest companiesRequest,
			UsernamePasswordAuthenticationToken token
	) {
		ExportRequest exportRequest = companiesRequest;

		if(companiesRequest.getId() != null) {
			try {
				SavedExport savedExport = SavedExportFactory.getExport(companiesRequest.getId());

				if(savedExport != null) {
					exportRequest = SavedExportUtil.buildExportRequest(savedExport, companiesRequest.isShouldSave());
				}
			} catch (SQLException throwables) {
				try {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				} catch (IOException e) {
					e.printStackTrace();
					//TODO log or something?
				}
			}
		}

		//If we don't have a list of ids, then we assume we need to get a list through a search query
		if(exportRequest.getIdList() == null) {
			CompaniesQuery companiesQuery = exportRequest.getCompaniesQuery();

			try {
				QueryResponse<CompaniesQueryHit> searchResponse = SearchUtil.doCompaniesSearch(companiesQuery);
				exportRequest.setIdList(searchResponse.getHits().stream().map(CompaniesQueryHit::getId).collect(Collectors.toList()));
			} catch (IOException e) {
				try {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				} catch (IOException ioException) {
					ioException.printStackTrace();
					//TODO log or something?
				}
			}
		}

		//If we've been requested to save the export for later use, then save it first
		if(exportRequest.isShouldSave()) {
			User user = (User) token.getPrincipal();
			SavedExport savedExport = SavedExportUtil.buildSavedExportFromExportRequest(
					exportRequest,
					ExportType.COMPANY,
					user.getId()
			);

			try {
				//TODO if this is an existing export request and we do want to save it, then update the data instead of inserting
				SavedExportFactory.insertSavedExport(savedExport);
			} catch (SQLException e) {
				e.printStackTrace();
				//TODO log and handle
			}
		}

		String fileName = exportRequest.getName() + ".csv";
		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename\""+fileName+"\"");

		try {
			StatefulBeanToCsv<ContactCSVEntry> csvWriter = new StatefulBeanToCsvBuilder<ContactCSVEntry>(response.getWriter())
					.withSeparator(CSVWriter.DEFAULT_SEPARATOR)
					.withOrderedResults(true)
					.build();

			csvWriter.write(ContactCSVFactory.getCompanyContactCSVEntries(exportRequest.getIdList()));
		} catch (CsvRequiredFieldEmptyException | IOException | CsvDataTypeMismatchException | SQLException e) {
			e.printStackTrace();
			//TODO log and handle properly
		}
	}
}
