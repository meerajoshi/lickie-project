package com.csv;

import java.util.HashMap;
import java.util.Map;

import com.outbound.search.company.Company;
import com.outbound.search.employee.Employee;
import com.outbound.search.industry.Industry;
import com.outbound.search.jobtitle.JobTitle;
import com.outbound.search.location.Location;
import com.outbound.search.person.Person;

public class ColumnMapper {

	enum ColumnType {
		FIRST_NAME, LAST_NAME, JOB_TITLE, SENIORITY, EMAIL, DIRECT_PHONE, CELL_PHONE, PERSON_LINKEDIN_PROFILE, PERSON_CITY, PERSON_STATE, PERSON_LINKEDIN_PHOTO, JOB_FUNCTION,
		COMPANY_NAME, COMPANY_CITY, COMPANY_STATE, COMPANY_ZIP, COMPANY_COUNTRY, COMPANY_DESCRIPTION, COMPANY_ADDRESS, CORPORATE_PHONE, EMPLOYEE_SIZE, INDUSTRY, COMPANY_LINKEDIN_URL, COMPANY_LOGO, COMPANY_WEBSITE
	}

	Map<Integer, ColumnType> columnTypeMap;

	public ColumnMapper(Integer firstName, Integer lastName, Integer jobTitle, Integer seniority, Integer email, Integer directPhone, Integer cellPhone, Integer personLinkedInProfile, Integer personCity, Integer personState, Integer personLinkedInPhoto, Integer jobFunction,
						Integer companyName, Integer companyCity, Integer companyState, Integer companyZip, Integer companyCountry, Integer companyDescription, Integer companyAddress, Integer corporatePhone, Integer employeeSize, Integer industry, Integer companyLinkedInUrl, Integer companyLogo, Integer companyWebsite) {
		columnTypeMap = new HashMap<>();

		//Person Columns
		columnTypeMap.put(firstName, ColumnType.FIRST_NAME);
		columnTypeMap.put(lastName, ColumnType.LAST_NAME);
		columnTypeMap.put(jobTitle, ColumnType.JOB_TITLE);
		columnTypeMap.put(seniority, ColumnType.SENIORITY);
		columnTypeMap.put(email, ColumnType.EMAIL);
		columnTypeMap.put(directPhone, ColumnType.DIRECT_PHONE);
		columnTypeMap.put(cellPhone, ColumnType.CELL_PHONE);
		columnTypeMap.put(personLinkedInProfile, ColumnType.PERSON_LINKEDIN_PROFILE);
		columnTypeMap.put(personCity, ColumnType.PERSON_CITY);
		columnTypeMap.put(personState, ColumnType.PERSON_STATE);
		columnTypeMap.put(personLinkedInPhoto, ColumnType.PERSON_LINKEDIN_PHOTO);
		columnTypeMap.put(jobFunction, ColumnType.JOB_FUNCTION);

		//Company Columns
		columnTypeMap.put(companyName, ColumnType.COMPANY_NAME);
		columnTypeMap.put(companyCity, ColumnType.COMPANY_CITY);
		columnTypeMap.put(companyState, ColumnType.COMPANY_STATE);
		columnTypeMap.put(companyZip, ColumnType.COMPANY_ZIP);
		columnTypeMap.put(companyCountry, ColumnType.COMPANY_COUNTRY);
		columnTypeMap.put(companyDescription, ColumnType.COMPANY_DESCRIPTION);
		columnTypeMap.put(companyAddress, ColumnType.COMPANY_ADDRESS);
		columnTypeMap.put(corporatePhone, ColumnType.CORPORATE_PHONE);
		columnTypeMap.put(employeeSize, ColumnType.EMPLOYEE_SIZE);
		columnTypeMap.put(industry, ColumnType.INDUSTRY);
		columnTypeMap.put(companyLinkedInUrl, ColumnType.COMPANY_LINKEDIN_URL);
		columnTypeMap.put(companyLogo, ColumnType.COMPANY_LOGO);
		columnTypeMap.put(companyWebsite, ColumnType.COMPANY_WEBSITE);
	}

	public static ColumnMapper getDefaultMapper() {
		return new ColumnMapper(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
				12, 13, 14, 15 ,16, 17, 18, 19, 20, 21, 22, 23, 24);
	}

	public static ColumnMapper getCompanyMapper() {
		return new ColumnMapper(null, null, null, null, null, null, null, null, null, null, null, null,
				1, 2, 3, null, null, null, 5, 6, 8, 13, 10, null, 0);
	}

	public void mapRowValues(String[] row, Person person, JobTitle jobTitle, Employee employee, Location personLocation, Industry industry, Company company, Location companyLocation) {
		for(int idx=0; idx<row.length; idx++) {
			String column = row[idx].trim();
			ColumnType columnType = this.columnTypeMap.get(idx);

			if(columnType != null) {
				switch (columnType) {
					case FIRST_NAME:
						person.setFirstName(column);
						break;
					case LAST_NAME:
						person.setLastName(column);
						break;
					case JOB_TITLE:
						jobTitle.setJobTitle(column);
						jobTitle.setDisplayName(column);
						break;
					case SENIORITY:
						employee.setSeniority(column);
						break;
					case EMAIL:
						person.setEmail(column);
						break;
					case DIRECT_PHONE:
						person.setDirectPhone(column);
						break;
					case CELL_PHONE:
						person.setCellPhone(column);
						break;
					case PERSON_LINKEDIN_PROFILE:
						person.setLinkedinProfile(column);
						break;
					case PERSON_CITY:
						personLocation.setCity(column);
						break;
					case PERSON_STATE:
						personLocation.setState(column);
						break;
					case PERSON_LINKEDIN_PHOTO:
						person.setLinkedinPhoto(column);
						break;
					case JOB_FUNCTION:
						employee.setFunction(column);
						break;
					case COMPANY_NAME:
						company.setName(column);
						break;
					case COMPANY_CITY:
						companyLocation.setCity(column);
						break;
					case COMPANY_STATE:
						companyLocation.setState(column);
						break;
					case COMPANY_ZIP:
						companyLocation.setPostal(column);
						break;
					case COMPANY_COUNTRY:
						companyLocation.setCountry(column);
						break;
					case COMPANY_DESCRIPTION:
						company.setDescription(column);
						break;
					case COMPANY_ADDRESS:
						companyLocation.setAddress(column);
						break;
					case CORPORATE_PHONE:
						company.setPhone(column);
						break;
					case EMPLOYEE_SIZE:
						company.setEmployeeSize(Integer.parseInt(column));
						break;
					case INDUSTRY:
						industry.setIndustry(column);
						industry.setDisplayName(column);
						break;
					case COMPANY_LINKEDIN_URL:
						company.setLinkedinProfile(column);
						break;
					case COMPANY_LOGO:
						company.setLogoUrl(column);
						break;
					case COMPANY_WEBSITE:
						company.setWebsiteUrl(column);
						break;
				}
			}
		}
	}
}
