package com.outbound.search.elasticsearch.querybuilders;

import com.outbound.search.queries.SimpleAutocompleteQuery;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.MultiMatchQueryBuilder.Type;

import static com.outbound.search.elasticsearch.Mappings.JobTitles;
import static com.outbound.search.elasticsearch.Mappings.Industries;

public class QueryBuilderUtils {
 public static QueryBuilder buildAutoCompleteQuery(String field, String query) {
  // See https://www.elastic.co/guide/en/elasticsearch/reference/7.x/search-as-you-type.html
  String[] fields = new String[] { field, field + "._2gram", field + "._3gram" };

  return QueryBuilders
    .multiMatchQuery(query, fields)
    .type(Type.BOOL_PREFIX);
 }

 public static SimpleAutocompleteQueryBuilder createJobTitleQueryBuilder(SimpleAutocompleteQuery query) {
   return new SimpleAutocompleteQueryBuilder(
     query,
     JobTitles.INDEX_NAME,
     JobTitles.ID_FIELD,
     JobTitles.JOB_TITLE_FIELD,
     JobTitles.DISPLAY_NAME_FIELD
    );
 }

 public static SimpleAutocompleteQueryBuilder createIndustryQueryBuilder(SimpleAutocompleteQuery query) {
  return new SimpleAutocompleteQueryBuilder(
    query,
    Industries.INDEX_NAME,
    Industries.ID_FIELD,
    Industries.INDUSTRY_FIELD,
    Industries.DISPLAY_NAME_FIELD
   );
  }
}
