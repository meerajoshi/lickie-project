package com.outbound.company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class CompanyFactory extends DBFactory {

	public Integer insertCompany(Company company) throws SQLException {
		try(Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("insert into company (name) values (?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, company.getName());
			statement.execute();

			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()){
				return rs.getInt(1);
			}
		}

		return null;
	}

	public List<Company> getAllCompanies() throws SQLException {
		List<Company> companies = new ArrayList<>();

		try(Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from company order by name");
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				companies.add(initCompanyFromRS(rs));
			}
		}

		return companies;
	}

	public static Company initCompanyFromRS(ResultSet rs) throws SQLException {
		Company company = new Company();

		company.setId(rs.getInt("company_id"));
		company.setName(rs.getString("name"));

		return company;
	}

}
