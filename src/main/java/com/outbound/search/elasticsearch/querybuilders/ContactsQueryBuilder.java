package com.outbound.search.elasticsearch.querybuilders;

import com.outbound.search.queries.ContactsQuery;
import com.outbound.search.queries.ContactsQueryHit;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import static com.outbound.search.elasticsearch.Mappings.Contacts;
import static com.outbound.search.elasticsearch.Mappings.Locations;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ContactsQueryBuilder implements IQueryBuilder<ContactsQueryHit> {
  private ContactsQuery queryDefinition;

  public ContactsQueryBuilder(ContactsQuery queryDefinition) {
    this.queryDefinition = queryDefinition;
  }

  @Override
  public String[] getSelectedFields() {
    return new String[] {
      Contacts.CONTACT_ID_FIELD,
      Contacts.EMPLOYEE_ID_FIELD,
      Contacts.NAME_FIELD,
      Contacts.EMAIL_FIELD,
      Contacts.SENIORITY_FIELD,
      Contacts.JOB_TITLE_FIELD,
      Contacts.LOCATION_FIELD + "." + Locations.STATE_FIELD,
      Contacts.LINKEDIN_PHOTO,
      Contacts.DIRECT_PHONE_FIELD,
      Contacts.CELL_PHONE_FIELD,
      Contacts.COMPANY_FIELD,
    };
  }

  @Override
  public String getIndexName() {
    return Contacts.INDEX_NAME;
  }

  @Override
  public int getOffset() {
    return this.queryDefinition.getOffset();
  }

  @Override
  public int getLimit() {
    return this.queryDefinition.getLimit();
  }

  @Override
  public ContactsQueryHit parseSearchHit(SearchHit searchHit) {
    Map<String, Object> hitSource = searchHit.getSourceAsMap();

    ContactsQueryHit hit = new ContactsQueryHit();

    hit.setId((int) hitSource.get(Contacts.CONTACT_ID_FIELD));
    hit.setEmployeeId((int) hitSource.get(Contacts.EMPLOYEE_ID_FIELD));
    hit.setName((String) hitSource.get(Contacts.NAME_FIELD));
    hit.setEmail((String) hitSource.get(Contacts.EMAIL_FIELD));
    hit.setSeniority((String) hitSource.get(Contacts.SENIORITY_FIELD));
    hit.setJobTitle((String) hitSource.get(Contacts.JOB_TITLE_FIELD));
    hit.setLinkedinPhoto((String) hitSource.get(Contacts.LINKEDIN_PHOTO));
    hit.setDirectPhone((String) hitSource.get(Contacts.DIRECT_PHONE_FIELD));
    hit.setCellPhone((String) hitSource.get(Contacts.CELL_PHONE_FIELD));
    hit.setCompany((String) hitSource.get(Contacts.COMPANY_FIELD));

    Map<String, Object> locationSource = (Map<String, Object>) hitSource.get(Contacts.LOCATION_FIELD);
    if (locationSource != null) {
      hit.setState((String) locationSource.get(Locations.STATE_FIELD));
    }

    return hit;
  }

  @Override
  public QueryBuilder buildRootQuery() {
    QueryBuilder matchNameOrEmail = buildNameOrEmailQuery();
    QueryBuilder haveRequiredContactFields = buildRequiredContactFieldsQuery();
    QueryBuilder matchJobTitleIds = buildJobTitleIdsQuery();
    QueryBuilder matchSeniority = buildSeniorityQuery();
    QueryBuilder matchHrFunction = buildHrFunctionQuery();
    QueryBuilder matchStates = buildStatesQuery();
    QueryBuilder matchCompanyIds = buildCompanyIdsQuery();

    if (matchNameOrEmail == null &&
        haveRequiredContactFields == null &&
        matchJobTitleIds == null &&
        matchSeniority == null &&
        matchHrFunction == null &&
        matchStates == null &&
        matchCompanyIds == null
      ) {
      return QueryBuilders.matchAllQuery();
    }

    BoolQueryBuilder rootQuery = QueryBuilders.boolQuery();

    if (matchNameOrEmail != null) {
      rootQuery.must(matchNameOrEmail);
    }

    if (haveRequiredContactFields != null) {
      rootQuery.filter(haveRequiredContactFields);
    }

    if (matchJobTitleIds != null) {
      rootQuery.filter(matchJobTitleIds);
    }

    if (matchSeniority != null) {
      rootQuery.filter(matchSeniority);
    }

    if (matchHrFunction != null) {
      rootQuery.filter(matchHrFunction);
    }

    if (matchStates != null) {
      rootQuery.filter(matchStates);
    }

    if (matchCompanyIds != null) {
      rootQuery.filter(matchCompanyIds);
    }

    return rootQuery;
  }

  private QueryBuilder buildNameOrEmailQuery() {
    String nameOrEmail = this.queryDefinition.getNameOrEmail();
    if (nameOrEmail == null || nameOrEmail.trim().length() == 0) {
      return null;
    }

    // Match on the name or the email.
    return QueryBuilders
      .boolQuery()
      .should(buildNameQuery())
      .should(buildEmailQuery())
      .minimumShouldMatch(1);
  }

  private QueryBuilder buildNameQuery() {
    return QueryBuilderUtils.buildAutoCompleteQuery(
      Contacts.NAME_FIELD,
      this.queryDefinition.getNameOrEmail()
    );
  }

  private QueryBuilder buildEmailQuery() {
    // Match on email, either exact match or match on a prefix to support
    // autocomplete.
    String value = this.queryDefinition.getNameOrEmail().toLowerCase();

    QueryBuilder matchExactly = QueryBuilders.termQuery(Contacts.EMAIL_FIELD, value).boost(2);
    QueryBuilder matchPrefix = QueryBuilders.prefixQuery(Contacts.EMAIL_FIELD, value);

    return QueryBuilders
      .boolQuery()
      .should(matchExactly)
      .should(matchPrefix)
      .minimumShouldMatch(1);
  }

  private QueryBuilder buildRequiredContactFieldsQuery() {
    String[] requiredFields = this.queryDefinition.getRequiredContactFields();
    if (requiredFields == null || requiredFields.length == 0) {
      return null;
    }

    List<QueryBuilder> existsQueries = Arrays.stream(requiredFields)
      .map(field -> QueryBuilders.existsQuery(field))
      .collect(Collectors.toList());

    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

    if (this.queryDefinition.getRequiredContactsFieldOp().equalsIgnoreCase("any")) {
      for (QueryBuilder existsQuery : existsQueries) {
        boolQuery.should(existsQuery);
      }
      boolQuery.minimumShouldMatch(1);
    } else {
      for (QueryBuilder existsQuery : existsQueries) {
        boolQuery.must(existsQuery);
      }
    }

    return boolQuery;
  }

  private QueryBuilder buildJobTitleIdsQuery() {
    int[] jobTitleIds = this.queryDefinition.getJobTitles();
    if (jobTitleIds == null || jobTitleIds.length == 0) {
      return null;
    }

    return QueryBuilders.termsQuery(Contacts.JOB_TITLE_ID_FIELD, jobTitleIds);
  }

  private QueryBuilder buildSeniorityQuery() {
    String[] seniority = this.queryDefinition.getSeniority();
    if (seniority == null || seniority.length == 0) {
      return null;
    }

    return QueryBuilders.termsQuery(Contacts.SENIORITY_FIELD, seniority);
  }

  private QueryBuilder buildHrFunctionQuery() {
    String[] hrFunction = this.queryDefinition.getHrFunction();
    if (hrFunction == null || hrFunction.length == 0) {
      return null;
    }

    return QueryBuilders.termsQuery(Contacts.FUNCTION_FIELD, hrFunction);
  }

  private QueryBuilder buildStatesQuery() {
    String[] states = this.queryDefinition.getStates();
    if (states == null || states.length == 0) {
      return null;
    }

    return QueryBuilders
      .termsQuery(Contacts.LOCATION_FIELD + "." + Locations.STATE_FIELD, states);
  }

  private QueryBuilder buildCompanyIdsQuery() {
    int[] companyIds = this.queryDefinition.getCompanyIds();
    if (companyIds == null || companyIds.length == 0) {
      return null;
    }

    return QueryBuilders.termsQuery(Contacts.COMPANY_ID_FIELD, companyIds);
  }
}
