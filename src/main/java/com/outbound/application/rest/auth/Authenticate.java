package com.outbound.application.rest.auth;

import java.io.Serializable;
import java.sql.SQLException;

import com.email.sendgrid.SendGridUtil;
import com.outbound.application.APIResponse;
import com.outbound.application.APIResponseBuilder;
import com.outbound.application.security.BCryptAuthenticator;
import com.outbound.user.User;
import com.outbound.user.UserFactory;
import com.outbound.user.UserInvite;
import com.outbound.user.UserInviteFactory;
import com.outbound.user.UserResetPasswordRequest;
import com.outbound.user.UserResetPasswordRequestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authenticate")
public class Authenticate implements Serializable {

    @Autowired
    private BCryptAuthenticator authenticator;

    @PostMapping("/create")
    @ResponseBody
    public APIResponse createUser(
        @RequestParam String username,
        @RequestParam String password,
        @RequestParam String email,
        @RequestParam String firstName,
        @RequestParam String lastName
    ) {
        AuthenticateResponseBuilder response = new AuthenticateResponseBuilder();
        boolean createdNewUser = false;
        UserFactory userFactory = new UserFactory();
        userFactory.init();

        try {
            if (userFactory.isUsernameUnique(username)) {
                User newUser = new User();
                newUser.setFirstName(firstName);
                newUser.setLastName(lastName);
                newUser.setEmail(email);
                newUser.setUsername(username);
                newUser.setPasswordHash(authenticator.hashPassword(password));

                newUser.setId(userFactory.insertUser(newUser));
                createdNewUser = true;
                response.withUser(newUser);
            } else {
                response.withMessage("Username is not unique");
            }
        } catch (SQLException e) {
            e.printStackTrace(); //TODO log this
        }

        response.withSuccess(createdNewUser);

        return response.build();
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/request-password-reset")
    public APIResponse requestResetPassword(
            @RequestParam String email
    ) {
        AuthenticateResponseBuilder response = new AuthenticateResponseBuilder();
        boolean success = true;
        UserFactory userFactory = new UserFactory();
        userFactory.init();

        try {
            User user = userFactory.getUserByEmail(email);

            if(user != null) {
                success = SendGridUtil.sendPasswordResetEmail(user);
                response.withUser(user);
            } else {
                success = false;
            }

            if(!success) {
                response.withMessage("Failed to send reset password email. Please try again later!");
            }
        } catch (SQLException throwables) {
            success = false;
            response.withMessage("An error occurred processing your request. Please try again later!");
            throwables.printStackTrace();//TODO log this
        }

        response.withSuccess(success);
        return response.build();
    }

    @PostMapping("/reset-password")
    public APIResponse resetPassword(
            @RequestParam(name = "u") String passwordRequestHash,
            @RequestParam(name = "i") Integer userId,
            @RequestParam String newPassword,
            @RequestParam String confirmPassword
    ) {
        AuthenticateResponseBuilder response = new AuthenticateResponseBuilder();
        boolean success = false;

        try {
            UserResetPasswordRequestFactory resetPasswordRequestFactory = new UserResetPasswordRequestFactory();
            resetPasswordRequestFactory.init();
            UserResetPasswordRequest passwordRequest = resetPasswordRequestFactory.getUserResetPasswordRequest(passwordRequestHash, userId);

            if(passwordRequest == null) {
                response.withMessage("Failed to reset your password. Please request a new link.");
            } else if (passwordRequest.isCompleted()) {
                response.withMessage("Failed to reset your password. Please request a new link.");
            } else if (!newPassword.equals(confirmPassword)) {
                response.withMessage("Failed to reset your password. The new password and confirm password do not match.");
            } else {
                UserFactory userFactory = new UserFactory();
                userFactory.init();
                User user = userFactory.getUserById(userId);

                if (user != null) {
                    userFactory.updateUserPassword(user, authenticator.hashPassword(newPassword));
                    success = true;

                    passwordRequest.setCompleted(true);
                    resetPasswordRequestFactory.updateRequest(passwordRequest);

                    response.withMessage("Password successfully updated! Please login with your new password.");
                } else {
                    response.withMessage("Failed to reset your password. Please request a new link.");
                }
            }
        } catch (SQLException throwables) {
            response.withMessage("An error occurred processing your request. Please try again later!");
            throwables.printStackTrace();//TODO log this
        }

        response.withSuccess(success);
        return response.build();
    }

    @PostMapping("/verify-password-reset-request")
    public APIResponse verifyResetPasswordRequest(
            @RequestParam(name = "u") String passwordRequestHash,
            @RequestParam(name = "i") Integer userId
    ) {
        AuthenticateResponseBuilder response = new AuthenticateResponseBuilder();
        boolean success = false;

        try {
            UserResetPasswordRequestFactory resetPasswordRequestFactory = new UserResetPasswordRequestFactory();
            resetPasswordRequestFactory.init();
            UserResetPasswordRequest passwordRequest = resetPasswordRequestFactory.getUserResetPasswordRequest(passwordRequestHash, userId);

            if(passwordRequest == null) {
                response.withMessage("Unable to reset password. Please request a new link.");
            } else if (passwordRequest.isCompleted()) {
                response.withMessage("Unable to reset password. Please request a new link.");
            } else {
                UserFactory userFactory = new UserFactory();
                userFactory.init();
                User user = userFactory.getUserById(userId);

                if (user != null) {
                    success = true;
                } else {
                    response.withMessage("Unable to reset password. Please request a new link.");
                }
            }
        } catch (SQLException throwables) {
            response.withMessage("An error occurred processing your request. Please try again later!");
            throwables.printStackTrace();//TODO log this
        }

        response.withSuccess(success);
        return response.build();
    }

    @PostMapping("/setup-account")
    public APIResponse setupAccount(
            @RequestParam(name = "u") String passwordRequestHash,
            @RequestParam(name = "i") Integer userId,
            @RequestParam String username,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String newPassword,
            @RequestParam String confirmPassword
    ) {
        AuthenticateResponseBuilder response = new AuthenticateResponseBuilder();
        boolean success = false;

        try {
            UserInviteFactory userInviteFactory = new UserInviteFactory();
            userInviteFactory.init();
            UserInvite userInvite = userInviteFactory.getUserInvite(passwordRequestHash, userId);

            UserFactory userFactory = new UserFactory();
            userFactory.init();

            if(userInvite == null) {
                response.withMessage("Failed to setup your account. Please request a new link.");
            } else if (userInvite.isCompleted()) {
                response.withMessage("Failed to setup your account. Please request a new link.");
            } else if (!newPassword.equals(confirmPassword)) {
                response.withMessage("Failed to setup your account. The new password and confirm password do not match.");
            } else if (!userFactory.isUsernameUnique(username)) {
                response.withMessage("Failed to setup your account. Username is not unique.");
            } else {
                User user = userFactory.getUserById(userId);

                if (user != null) {
                    user.setPasswordHash(authenticator.hashPassword(newPassword));
                    user.setUsername(username);
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    userFactory.updateUser(user);

                    success = true;

                    userInvite.setCompleted(true);
                    userInviteFactory.updateInvite(userInvite);

                    response.withMessage("User successfully setup! Please login with your new account.");
                } else {
                    response.withMessage("Failed to setup your account. Please request a new link.");
                }
            }
        } catch (SQLException throwables) {
            response.withMessage("An error occurred processing your request. Please try again later!");
            throwables.printStackTrace();//TODO log this
        }

        response.withSuccess(success);
        return response.build();
    }

    @PostMapping("/setup-account/verify")
    public APIResponse verifyUserInvite(
            @RequestParam(name = "u") String inviteHash,
            @RequestParam(name = "i") Integer userId
    ) {
        AuthenticateResponseBuilder response = new AuthenticateResponseBuilder();
        boolean success = false;

        try {
            UserInviteFactory userInviteFactory = new UserInviteFactory();
            userInviteFactory.init();
            UserInvite userInvite = userInviteFactory.getUserInvite(inviteHash, userId);

            if(userInvite == null) {
                response.withMessage("Unable to setup account. Please request a new link.");
            } else if (userInvite.isCompleted()) {
                response.withMessage("Unable to setup account. Please request a new link.");
            } else {
                UserFactory userFactory = new UserFactory();
                userFactory.init();
                User user = userFactory.getUserById(userId);

                if (user != null) {
                    success = true;
                } else {
                    response.withMessage("Unable to setup account. Please request a new link.");
                }
            }
        } catch (SQLException throwables) {
            response.withMessage("An error occurred processing your request. Please try again later!");
            throwables.printStackTrace();//TODO log this
        }

        response.withSuccess(success);
        return response.build();
    }
}

class AuthenticateResponseBuilder extends APIResponseBuilder {
    private User user;

    public User getUser() {
        return user;
    }

    public AuthenticateResponseBuilder withUser(User user) {
        this.user = user;
        return this;
    }

    @Override
    public APIResponse build() {
        APIResponse response = new APIResponse();

        response.setData(new AuthenticateData(user));
        response.setSuccess(this.isSuccess());
        response.setMessage(this.getMessage());

        return response;
    }

    public class AuthenticateData implements Serializable {
        private User user;

        AuthenticateData(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }
    }
}