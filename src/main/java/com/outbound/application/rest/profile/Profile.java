package com.outbound.application.rest.profile;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.email.sendgrid.SendGridUtil;
import com.outbound.application.APIResponse;
import com.outbound.application.GenericAPIResponseBuilder;
import com.outbound.application.rest.auth.UserModel;
import com.outbound.application.security.BCryptAuthenticator;
import com.outbound.user.User;
import com.outbound.user.UserFactory;
import com.outbound.user.UserInvite;
import com.outbound.user.UserInviteFactory;
import com.outbound.user.UserRole;
import com.outbound.user.UserRoleFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/profile")
@PreAuthorize("isAuthenticated()")
public class Profile {

	@Autowired
	private BCryptAuthenticator authenticator;

	@PostMapping(
			value = "/update-user",
			consumes = { MediaType.APPLICATION_JSON_VALUE },
			produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public APIResponse updateUser(@RequestBody UpdateUser updateUser) throws SQLException {
		//TODO ensure that the user editing this user is the user themselves!
		if(updateUser != null && updateUser.getId() != null) {
			final boolean isPasswordUpdate = updateUser.getNewPassword() != null && updateUser.getConfirmPassword() != null;

			if(isPasswordUpdate && !updateUser.getNewPassword().equals(updateUser.getConfirmPassword())) {
				return new GenericAPIResponseBuilder().withMessage("Failed to reset your password. The new password and confirm password do not match.").withSuccess(false).build();
			} else if (
				updateUser.getEmail() == null
				|| updateUser.getEmail().trim().length() == 0
				|| updateUser.getFirstName() == null
				|| updateUser.getFirstName().trim().length() == 0
				|| updateUser.getLastName() == null
				|| updateUser.getLastName().trim().length() == 0
			) {
				return new GenericAPIResponseBuilder().withMessage("Not a valid user.").withSuccess(false).build();
			} else {
				UserFactory userFactory = new UserFactory();

				User user = userFactory.getUserById(updateUser.getId());
				user.setFirstName(updateUser.getFirstName());
				user.setLastName(updateUser.getLastName());
				user.setEmail(updateUser.getEmail());
				userFactory.updateUser(user);

				if(isPasswordUpdate) {
					userFactory.updateUserPassword(user, authenticator.hashPassword(updateUser.getNewPassword()));
				}

				return new GenericAPIResponseBuilder().withMessage("Successfully updated user!").withSuccess(true).build();
			}
		} else {
			return new GenericAPIResponseBuilder().withMessage("Not a valid user.").withSuccess(false).build();
		}
	}

	@PostMapping("/invite")
	@PreAuthorize("hasAnyRole('SUPER_USER', 'INTERNAL', 'ROLE_ADMIN')")
	@ResponseBody
	public APIResponse inviteUser(
			@RequestParam String email,
			@RequestParam String role,
			@RequestParam Integer companyId,
			UsernamePasswordAuthenticationToken token
	) {
		GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();
		boolean success = false;
		User invitingUser = (User) token.getPrincipal();

		if(invitingUser != null && email != null && !email.trim().equals("") && role != null) {
			UserFactory userFactory = new UserFactory();

			try {
				if(userFactory.isEmailUnique(email)) {
					UserRoleFactory roleFactory = new UserRoleFactory();

					User newUser = new User();
					newUser.setEmail(email);
					newUser.setUserRoleId(roleFactory.getUserRole(UserRole.valueOf(role)).getRoleId());
					newUser.setCompanyId(companyId);
					newUser.setId(userFactory.insertUser(newUser));

					if(newUser.getId() != null) {
						UserInviteFactory userInviteFactory = new UserInviteFactory();
						userInviteFactory.init();

						UserInvite userInvite = new UserInvite();
						userInvite.setUserId(newUser.getId());
						userInvite.setHash(userInviteFactory.insertNewUserInvite(userInvite, newUser, invitingUser.getId()));

						success = true;
						SendGridUtil.sendUserInviteEmail(invitingUser, userInvite, email);
						responseBuilder.withMessage("User invite sent to " + email + ".");
					}
				} else {
					responseBuilder.withMessage("User email is not unique.");
				}
			} catch (SQLException throwables) {
				throwables.printStackTrace();
				//TODO log
				responseBuilder.withMessage("Server error. Please try again later.");
			}
		} else {
			responseBuilder.withMessage("No email given.");
		}

		responseBuilder.withSuccess(success);
		return responseBuilder.build();
	}

	@PostMapping("/delete")
	@PreAuthorize("hasAnyRole('SUPER_USER', 'INTERNAL', 'ROLE_ADMIN')")
	@ResponseBody
	public APIResponse getSystemUsers(
			@RequestParam int userId,
			UsernamePasswordAuthenticationToken token
	) {
		GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();
		UserFactory userFactory = new UserFactory();

		userFactory.deleteUser(userId);
		responseBuilder.withSuccess(true);
		return responseBuilder.build();
	}

	@PreAuthorize("hasAnyRole('SUPER_USER', 'INTERNAL', 'ROLE_ADMIN')")
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public APIResponse getProfileInfo(UsernamePasswordAuthenticationToken token) throws SQLException {
		final User user = (User) token.getPrincipal();

		UserFactory userFactory = new UserFactory();
		UserInviteFactory userInviteFactory = new UserInviteFactory();

		final List<User> companyUsers = userFactory.getUsersByCompany(user.getCompanyId());
		final List<UserInvite> invites = userInviteFactory.getUserInvitesForCompany(user.getCompanyId());
		final List<UserInviteInfo> inviteInfos = new ArrayList<>();
		final List<Integer> inviteUserIds = new ArrayList<>();

		invites.forEach(invite -> {
			if(!invite.getUserId().equals(user.getId()) && !invite.isCompleted()) {
				User userFromInvite = companyUsers.stream().filter(cUser -> cUser.getId().equals(invite.getUserId())).findFirst().get();
				inviteUserIds.add(invite.getUserId());
				inviteInfos.add(new UserInviteInfo(invite, userFromInvite));
			}
		});

		final List<UserModel> users = new ArrayList<>();
		UserRoleFactory userRoleFactory = new UserRoleFactory();
		userRoleFactory.init();

		final List<UserRole> roles = userRoleFactory.getUserRoles();

		companyUsers.forEach(cUser -> {
			if(!inviteUserIds.contains(cUser.getId()) && !cUser.getId().equals(user.getId())) {
				UserRole userRole = roles.stream().filter(role -> cUser.getUserRoleId().equals(role.getRoleId())).findFirst().get();
				users.add(new UserModel(cUser, userRole));
			}
		});

		ProfileInfo profileInfo = new ProfileInfo(users, inviteInfos);
		return new GenericAPIResponseBuilder().withData(profileInfo).withSuccess(true).build();
	}

	static class ProfileInfo {
		List<UserModel> companyUsers;
		List<UserInviteInfo> invites;

		public ProfileInfo(List<UserModel> companyUsers, List<UserInviteInfo> invites) {
			this.companyUsers = companyUsers;
			this.invites = invites;
		}

		public List<UserModel> getCompanyUsers() {
			return companyUsers;
		}

		public void setCompanyUsers(List<UserModel> companyUsers) {
			this.companyUsers = companyUsers;
		}

		public List<UserInviteInfo> getInvites() {
			return invites;
		}

		public void setInvites(List<UserInviteInfo> invites) {
			this.invites = invites;
		}
	}
}
