package com.outbound.application.rest.contact;

import com.outbound.application.APIResponse;
import com.outbound.application.GenericAPIResponseBuilder;
import com.outbound.application.rest.model.ContactDetails;
import com.outbound.application.rest.model.ContactDetailsUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contacts")
@PreAuthorize("isAuthenticated()")
public class Contact {

	@GetMapping("/get")
	@ResponseBody
	public APIResponse getContact(@RequestParam(name = "contact") Integer id) {
		ContactDetails contactDetails = ContactDetailsUtil.getContactDetails(id);
		return new GenericAPIResponseBuilder().withData(contactDetails).build();
	}

}
