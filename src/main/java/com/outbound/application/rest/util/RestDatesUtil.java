package com.outbound.application.rest.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RestDatesUtil {

	//TODO unit test
	public static String getLocalDateString(LocalDate localDate) {
		if(localDate != null) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			return dtf.format(localDate);
		}

		return null;
	}

	//TODO unit test
	public static String getLocalDateTimeString(LocalDateTime localDateTime) {
		if(localDateTime != null) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			return dtf.format(localDateTime);
		}

		return null;
	}

}
