package com.outbound.search.elasticsearch;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.outbound.application.configuration.ElasticsearchClientConfig;
import com.outbound.search.company.Company;
import com.outbound.search.company.CompanyFactory;
import com.outbound.search.employee.Employee;
import com.outbound.search.employee.EmployeeFactory;
import com.outbound.search.industry.Industry;
import com.outbound.search.industry.IndustryFactory;
import com.outbound.search.jobtitle.JobTitle;
import com.outbound.search.jobtitle.JobTitleFactory;
import com.outbound.search.location.Location;
import com.outbound.search.location.LocationFactory;
import com.outbound.search.person.Person;
import com.outbound.search.person.PersonFactory;
import static com.outbound.search.elasticsearch.Mappings.Companies;
import static com.outbound.search.elasticsearch.Mappings.Contacts;
import static com.outbound.search.elasticsearch.Mappings.JobTitles;
import static com.outbound.search.elasticsearch.Mappings.Industries;

import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;

public class Indexer {
  private Map<Integer, Person> people;
  private Map<Integer, Employee> employees;
  private Map<Integer, Location> locations;
  private Map<Integer, Company> companies;
  private Map<Integer, JobTitle> jobTitles;
  private Map<Integer, Industry> industries;

  private RestHighLevelClient elasticsearchClient;

  public void indexAll() throws IOException, IndexerException {
    indexAllContacts();
    indexAllCompanies();
    indexAllIndustries();
    indexAllJobTitles();
  }

  private void indexAllCompanies() throws IOException, IndexerException {
    Map<Integer, Location> locations = getLocations();
    Map<Integer, Company> companies = getCompanies();
    Map<Integer, Industry> industries = getIndustries();

    BulkRequest bulkIndexRequest = new BulkRequest(Companies.INDEX_NAME);
    for (Company company : companies.values()) {
      Location location = locations.get(company.getLocationId());
      Industry industry = industries.get(company.getIndustryId());
      Map<String, Object> serializedCompany = DocumentSerializer.serializeCompany(company, location, industry);

      // Build the index request for this document with DocWriteRequest.OpType.CREATE
      // to only index the document if it does not already exist. Also use the
      // company primary key as the id for the document in Elasticsearch.
      IndexRequest indexRequest = new IndexRequest(Companies.INDEX_NAME)
        .id(Integer.toString(company.getId()))
        .source(serializedCompany)
        .opType(DocWriteRequest.OpType.CREATE);

      bulkIndexRequest.add(indexRequest);
    }

    BulkResponse response = getElasticsearchClient().bulk(bulkIndexRequest, RequestOptions.DEFAULT);

    if(response.hasFailures()) {
      throw new IndexerException("Failed to index companies!", response);
    }
  }

  private void indexAllIndustries() throws IOException, IndexerException {
    Map<Integer, Industry> industries = getIndustries();

    BulkRequest bulkIndexRequest = new BulkRequest(Industries.INDEX_NAME);
    for (Industry industry : industries.values()) {
      Map<String, Object> serializedIndustry = DocumentSerializer.serializeIndustry(industry);

      IndexRequest indexRequest = new IndexRequest(Industries.INDEX_NAME)
        .id(Integer.toString(industry.getId()))
        .source(serializedIndustry)
        .opType(DocWriteRequest.OpType.CREATE);

      bulkIndexRequest.add(indexRequest);
    }

    BulkResponse response = getElasticsearchClient().bulk(bulkIndexRequest, RequestOptions.DEFAULT);

    if(response.hasFailures()) {
      throw new IndexerException("Failed to index industries!", response);
    }
  }

  private void indexAllContacts() throws IOException, IndexerException {
    Map<Integer, Employee> employees = getEmployees();
    Map<Integer, Company> companies = getCompanies();
    Map<Integer, Location> locations = getLocations();
    Map<Integer, Person> people = getPeople();
    Map<Integer, JobTitle> jobTitles = getJobTitles();

    BulkRequest bulkIndexRequest = new BulkRequest(Contacts.INDEX_NAME);
    for (Employee employee : employees.values()) {
      Person person = people.get(employee.getContactId());
      Location location = locations.get(person.getLocationId());
      JobTitle jobTitle = jobTitles.get(employee.getJobTitleId());
      Company company = companies.get(employee.getCompanyId());

      Map<String, Object> serializedContact = DocumentSerializer.serializeContact(
        employee,
        person,
        location,
        jobTitle,
        company
      );

      // Build the index request for this document with DocWriteRequest.OpType.CREATE
      // to only index the document if it does not already exist. Also use the
      // employee primary key as the id for the document in Elasticsearch.
      IndexRequest indexRequest = new IndexRequest(Contacts.INDEX_NAME)
        .id(Integer.toString(employee.getId()))
        .source(serializedContact)
        .opType(DocWriteRequest.OpType.CREATE);

      bulkIndexRequest.add(indexRequest);
    }

    BulkResponse response = getElasticsearchClient().bulk(bulkIndexRequest, RequestOptions.DEFAULT);

    if(response.hasFailures()) {
      throw new IndexerException("Failed to index contacts!", response);
    }
  }

  private void indexAllJobTitles() throws IOException, IndexerException {
    Map<Integer, JobTitle> jobTitles = getJobTitles();

    BulkRequest bulkIndexRequest = new BulkRequest(JobTitles.INDEX_NAME);
    for (JobTitle jobTitle : jobTitles.values()) {
      Map<String, Object> serializedJobTitle = DocumentSerializer.serializeJobTitle(jobTitle);

      IndexRequest indexRequest = new IndexRequest(JobTitles.INDEX_NAME)
        .id(Integer.toString(jobTitle.getId()))
        .source(serializedJobTitle)
        .opType(DocWriteRequest.OpType.CREATE);

      bulkIndexRequest.add(indexRequest);
    }

    BulkResponse response = getElasticsearchClient().bulk(bulkIndexRequest, RequestOptions.DEFAULT);

    if(response.hasFailures()) {
      throw new IndexerException("Failed to index job titles!", response);
    }
  }

  // TODO: This should be configured via dependency injection.
  private RestHighLevelClient getElasticsearchClient() {
    if (this.elasticsearchClient != null) {
      return this.elasticsearchClient;
    }

    this.elasticsearchClient = new ElasticsearchClientConfig().elasticsearchClient();
    return this.elasticsearchClient;
  }

  private Map<Integer, Company> getCompanies() {
    if (this.companies != null) {
      return this.companies;
    }

    CompanyFactory factory = new CompanyFactory();
    factory.init();

    List<Company> records = factory.getAllCompanies();
    Map<Integer, Company> idToRecord = new HashMap<>();
    for (Company record : records) {
      idToRecord.put(record.getId(), record);
    }

    this.companies = idToRecord;
    return this.companies;
  }

  private Map<Integer, Employee> getEmployees() {
    if (this.employees != null) {
      return this.employees;
    }

    EmployeeFactory factory = new EmployeeFactory();
    factory.init();

    List<Employee> records = factory.getAllEmployees();
    Map<Integer, Employee> idToRecord = new HashMap<>();
    for (Employee record : records) {
      idToRecord.put(record.getId(), record);
    }

    this.employees = idToRecord;
    return this.employees;
  }

  private Map<Integer, Location> getLocations() {
    if (this.locations != null) {
      return this.locations;
    }

    LocationFactory factory = new LocationFactory();
    factory.init();

    List<Location> records = factory.getAllLocations();
    Map<Integer, Location> idToRecord = new HashMap<>();
    for (Location record : records) {
      idToRecord.put(record.getId(), record);
    }

    this.locations = idToRecord;
    return this.locations;
  }

  private Map<Integer, Person> getPeople() {
    if (this.people != null) {
      return this.people;
    }

    List<Person> records = PersonFactory.getAllPersons();
    Map<Integer, Person> idToRecord = new HashMap<>();
    for (Person record : records) {
      idToRecord.put(record.getId(), record);
    }

    this.people = idToRecord;
    return this.people;
  }

  private Map<Integer, JobTitle> getJobTitles() {
    if (this.jobTitles != null) {
      return this.jobTitles;
    }

    JobTitleFactory factory = new JobTitleFactory();
    factory.init();

    List<JobTitle> records = factory.getAllJobTitles();
    Map<Integer, JobTitle> idToRecord = new HashMap<>();
    for (JobTitle record : records) {
      idToRecord.put(record.getId(), record);
    }

    this.jobTitles = idToRecord;
    return this.jobTitles;
  }

  private Map<Integer, Industry> getIndustries() {
    if (this.industries != null) {
      return this.industries;
    }

    IndustryFactory factory = new IndustryFactory();
    factory.init();

    List<Industry> records = factory.getAllIndustries();
    Map<Integer, Industry> idToRecord = new HashMap<>();
    for (Industry record : records) {
      idToRecord.put(record.getId(), record);
    }

    this.industries = idToRecord;
    return this.industries;
  }
}
