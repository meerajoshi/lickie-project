package com.outbound.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnector {

    private static final String connectionUrl = System.getenv("connectionUrl");
    private static final String user = System.getenv("dbUser");
    private static final String password = System.getenv("dbPassword");

    public static Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, "Error loading the db driver!", e);
        }

        return DriverManager.getConnection(connectionUrl, user, password);
    }
}
