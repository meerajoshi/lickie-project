package com.outbound.slack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.block.LayoutBlock;

public class SlackInteractiveMessage {

    List<LayoutBlock> blocks = new ArrayList<>();
    List<Attachment> attachments = new ArrayList<>();

    public List<LayoutBlock> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<LayoutBlock> blocks) {
        this.blocks = blocks;
    }

    public SlackInteractiveMessage addBlocks(List<LayoutBlock> blocks) {
        this.blocks.addAll(blocks);

        return this;
    }

    public void sendMessage(String webhookUrl) throws IOException {
        SlackWebhooksUtil.send(webhookUrl, this.blocks, this.attachments);
    }


    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public SlackInteractiveMessage addAttachments(List<Attachment> attachments) {
        this.attachments.addAll(attachments);
        return this;
    }
}
