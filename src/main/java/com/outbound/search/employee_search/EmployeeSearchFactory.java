package com.outbound.search.employee_search;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class EmployeeSearchFactory extends DBFactory{
	public static List<EmployeeSearch> getEmployeesByCompanyId(int id) {
		List<EmployeeSearch> employeeList = new ArrayList<>();

		try (Connection connection = DBConnector.getConnection()) {
			String Query="SELECT search_person.search_person_id,search_person.first_name,search_person.last_name,search_employee.function,search_job_title.display_name from search_person,search_employee,search_job_title,search_company where search_job_title.search_job_title_id=search_employee.search_job_title_id AND search_employee.search_company_id in ("+id+") AND search_company.search_company_id in ("+id+")AND search_person.search_person_id=search_employee_id;";
			PreparedStatement statement = connection.prepareStatement(Query);

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				employeeList.add(initEmployeeFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return employeeList;
	}
	private static EmployeeSearch initEmployeeFromRS(ResultSet rs) throws SQLException {
		EmployeeSearch employee = new EmployeeSearch();
		
		employee.setId(rs.getInt("search_person_id"));
		employee.setFirstName(rs.getString("first_name"));
		employee.setLastName(rs.getString("last_name"));
		employee.setDisplayName(rs.getString("display_name"));
		employee.setFunction(rs.getString("function"));

		return employee;
	}
}
