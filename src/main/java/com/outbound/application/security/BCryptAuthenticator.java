package com.outbound.application.security;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

public class BCryptAuthenticator implements PasswordEncoder {

    private final int saltRounds;

    public BCryptAuthenticator(int saltRounds) {
        this.saltRounds = saltRounds;
    }

    public String hashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(this.saltRounds));
    }

    public boolean verifyPassword(String password, String hash) {
        return BCrypt.checkpw(password, hash);
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return hashPassword(rawPassword.toString());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return verifyPassword(rawPassword.toString(), encodedPassword);
    }
}
