package com.outbound.export;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.outbound.database.DBConnector;
import com.outbound.model.TrackedDataModel;

public class SavedExportFactory {

	public static Integer insertSavedExport(SavedExport export) throws SQLException {
		Integer id = null;
		String query = "insert into saved_export (name, description, type, ids, original_search_query, created_by) values (?,?,?,?,?,?)";

		try(Connection c = DBConnector.getConnection(); PreparedStatement ps = c.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			int psIdx = 1;
			ps.setString(psIdx++, export.getName());
			ps.setString(psIdx++, export.getDescription());
			ps.setString(psIdx++, export.getType().name());

			Gson gson = new Gson();
			ps.setString(psIdx++, gson.toJson(export.getIds()));

			ps.setString(psIdx++, export.getOriginalSearchQuery());
			ps.setInt(psIdx++, export.getCreatedBy());
			ps.execute();

			try (ResultSet rs = ps.getGeneratedKeys()) {
				rs.next();
				id = rs.getInt(1);
			}
		}

		return id;
	}

	public static void deleteSavedExport(int exportId) throws SQLException {
		String query = "delete from saved_export where saved_export_id = ?";

		try (Connection c = DBConnector.getConnection(); PreparedStatement ps = c.prepareStatement(query)) {
			ps.setInt(1, exportId);
			ps.executeUpdate();
		}
	}

	public static void renameSavedExport(int exportId, String name) throws SQLException {
		String query = "update saved_export set name = ? where saved_export_id = ?";

		try(Connection c = DBConnector.getConnection(); PreparedStatement ps = c.prepareStatement(query)) {
			ps.setString(1, name);
			ps.setInt(2, exportId);
			ps.executeUpdate();
		}
	}

	public static SavedExport getExport(int exportId) throws SQLException {
		SavedExport export = null;
		String query = "select * from saved_export where saved_export_id = ?";

		try(Connection c = DBConnector.getConnection(); PreparedStatement ps = c.prepareStatement(query)) {
			ps.setInt(1, exportId);

			try (ResultSet rs = ps.executeQuery()) {
				if(rs.next()) {
					export = initSavedExportFromRS(rs);
				}
			}
		}

		return export;
	}

	public static List<SavedExport> getUserExports(int userId) throws SQLException {
		List<SavedExport> exports = new ArrayList<>();
		String query = "select * from saved_export where created_by = ? order by created_on desc";

		try(Connection c = DBConnector.getConnection(); PreparedStatement ps = c.prepareStatement(query)) {
			ps.setInt(1, userId);

			try (ResultSet rs = ps.executeQuery()) {
				while(rs.next()) {
					exports.add(initSavedExportFromRS(rs));
				}
			}
		}

		return exports;
	}

	private static SavedExport initSavedExportFromRS(ResultSet rs) throws SQLException {
		SavedExport export = new SavedExport();

		export.setId(rs.getInt("saved_export_id"));
		export.setName(rs.getString("name"));
		export.setDescription(rs.getString("description"));
		export.setType(ExportType.valueOf(rs.getString("type")));
		export.setOriginalSearchQuery(rs.getString("original_search_query"));

		if(rs.getString("ids") != null) {
			Gson gson = new Gson();
			Type listOfIntegersType = new TypeToken<ArrayList<Integer>>() {}.getType();
			export.setIds(gson.fromJson(rs.getString("ids"), listOfIntegersType));
		}

		TrackedDataModel.initTrackedDataModelFromRS(rs, export);

		return export;
	}

}
