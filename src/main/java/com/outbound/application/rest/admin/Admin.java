package com.outbound.application.rest.admin;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.csv.ColumnMapper;
import com.email.sendgrid.SendGridUtil;
import com.opencsv.CSVReader;
import com.outbound.application.APIResponse;
import com.outbound.application.GenericAPIResponseBuilder;
import com.outbound.application.rest.auth.UserModel;
import com.outbound.database.DBConnector;
import com.outbound.search.company.Company;
import com.outbound.search.company.CompanyFactory;
import com.outbound.search.elasticsearch.Indexer;
import com.outbound.search.elasticsearch.IndexerException;
import com.outbound.search.employee.Employee;
import com.outbound.search.employee.EmployeeFactory;
import com.outbound.search.employee.EmploymentStatus;
import com.outbound.search.industry.Industry;
import com.outbound.search.industry.IndustryFactory;
import com.outbound.search.jobtitle.JobTitle;
import com.outbound.search.jobtitle.JobTitleFactory;
import com.outbound.search.location.Location;
import com.outbound.search.location.LocationFactory;
import com.outbound.search.person.Person;
import com.outbound.search.person.PersonFactory;
import com.outbound.user.User;
import com.outbound.user.UserFactory;
import com.outbound.user.UserInvite;
import com.outbound.user.UserInviteFactory;
import com.outbound.user.UserRole;
import com.outbound.user.UserRoleFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/admin")
@PreAuthorize("isAuthenticated()")
public class Admin implements Serializable {

	@PostMapping("/invite")
	@PreAuthorize("hasAnyRole('SUPER_USER', 'INTERNAL_ADMIN', 'INTERNAL')")
	@ResponseBody
	public APIResponse inviteUser(
		@RequestParam String email,
		@RequestParam String role,
		@RequestParam Integer companyId,
		UsernamePasswordAuthenticationToken token
	) {
		GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();
		boolean success = false;
		User invitingUser = (User) token.getPrincipal();

		if(invitingUser != null && email != null && !email.trim().equals("") && role != null) {
			UserFactory userFactory = new UserFactory();
			userFactory.init();

			try {
				if(userFactory.isEmailUnique(email)) {
					UserRoleFactory roleFactory = new UserRoleFactory();
					roleFactory.init();

					User newUser = new User();
					newUser.setEmail(email);
					newUser.setUserRoleId(roleFactory.getUserRole(UserRole.valueOf(role)).getRoleId());
					newUser.setCompanyId(companyId);
					newUser.setId(userFactory.insertUser(newUser));

					if(newUser.getId() != null) {
						UserInviteFactory userInviteFactory = new UserInviteFactory();
						userInviteFactory.init();

						UserInvite userInvite = new UserInvite();
						userInvite.setUserId(newUser.getId());
						userInvite.setHash(userInviteFactory.insertNewUserInvite(userInvite, newUser, invitingUser.getId()));

						success = true;
						SendGridUtil.sendUserInviteEmail(invitingUser, userInvite, email);
						responseBuilder.withMessage("User invite sent to " + email + ".");
					}
				} else {
					responseBuilder.withMessage("User email is not unique.");
				}
			} catch (SQLException throwables) {
				throwables.printStackTrace();
				//TODO log
				responseBuilder.withMessage("Server error. Please try again later.");
			}
		} else {
			responseBuilder.withMessage("No email given.");
		}

		responseBuilder.withSuccess(success);
		return responseBuilder.build();
	}

	@GetMapping("/users")
	@PreAuthorize("hasAnyRole('INTERNAL', 'INTERNAL_ADMIN', 'SUPER_USER')")
	@ResponseBody
	public APIResponse getSystemUsers(
			UsernamePasswordAuthenticationToken token
	) {
		GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();
		UserRoleFactory userRoleFactory = new UserRoleFactory();
		userRoleFactory.init();

		Map<Integer, UserRole> userRoleMap = new HashMap<>();
		for(UserRole role : userRoleFactory.getUserRoles()) {
			userRoleMap.put(role.getRoleId(), role);
		}

		UserFactory userFactory = new UserFactory();
		userFactory.init();

		List<UserModel> systemUsers = userFactory.getUsers().stream()
				.map(user -> new UserModel(user, userRoleMap.get(user.getUserRoleId())))
				.collect(Collectors.toList());

		responseBuilder.withData(systemUsers);
		responseBuilder.withSuccess(true);
		return responseBuilder.build();
	}

	@PostMapping("/delete")
	@PreAuthorize("hasAnyRole('INTERNAL', 'INTERNAL_ADMIN', 'SUPER_USER')")
	@ResponseBody
	public APIResponse deleteUser(
			@RequestParam int userId,
			UsernamePasswordAuthenticationToken token
	) {
		GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();
		UserRoleFactory userRoleFactory = new UserRoleFactory();
		userRoleFactory.init();

		Map<Integer, UserRole> userRoleMap = new HashMap<>();
		for(UserRole role : userRoleFactory.getUserRoles()) {
			userRoleMap.put(role.getRoleId(), role);
		}

		UserFactory userFactory = new UserFactory();
		userFactory.init();

		userFactory.deleteUser(userId);

		List<UserModel> systemUsers = userFactory.getUsers().stream()
				.map(user -> new UserModel(user, userRoleMap.get(user.getUserRoleId())))
				.collect(Collectors.toList());

		responseBuilder.withData(systemUsers);
		responseBuilder.withSuccess(true);
		return responseBuilder.build();
	}

	@PostMapping("/data/import")
	@PreAuthorize("hasRole('ROLE_SUPER_USER')")
	@ResponseBody
	public APIResponse importData(
			@RequestParam(required = false) Boolean companiesOnly,
			@RequestParam MultipartFile importFile
			) {
		boolean success = false;
		int dataCount = 0;

		try {
			if(companiesOnly != null && companiesOnly) {
				dataCount = importCompanies(importFile);
			} else {
				dataCount = importSearchData(importFile);
			}

			success = true;
		} catch (IOException e) {
			e.printStackTrace();
			//TODO log
		}

		return new GenericAPIResponseBuilder().withData(dataCount).withSuccess(success).build();
	}

	@PostMapping("/company/insert")
	@PreAuthorize("hasAnyRole('INTERNAL', 'SUPER_USER')")
	@ResponseBody
	public APIResponse insertCompany(
	 	@RequestParam String name,
		UsernamePasswordAuthenticationToken token
	) throws SQLException {
		if(name != null && name.trim().length() > 0) {
			com.outbound.company.CompanyFactory companyFactory = new com.outbound.company.CompanyFactory();
			companyFactory.init();

			com.outbound.company.Company newCompany = new com.outbound.company.Company();
			newCompany.setName(name);

			companyFactory.insertCompany(newCompany);

			List<com.outbound.company.Company> companies = companyFactory.getAllCompanies();
			return new GenericAPIResponseBuilder().withData(companies).withSuccess(true).withMessage("Company added!").build();
		}

		return new GenericAPIResponseBuilder().withMessage("Failed to add company!").withSuccess(false).build();
	}

	@GetMapping(
			value = "/companies",
			produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	@PreAuthorize("hasAnyRole('INTERNAL', 'INTERNAL_ADMIN', 'SUPER_USER')")
	public APIResponse getAllCompanies() throws SQLException {
		com.outbound.company.CompanyFactory companyFactory = new com.outbound.company.CompanyFactory();
		companyFactory.init();

		List<com.outbound.company.Company> companies = companyFactory.getAllCompanies();
		return new GenericAPIResponseBuilder().withData(companies).withSuccess(true).build();
	}

	@PostMapping("/data/delete")
	@PreAuthorize("hasRole('ROLE_SUPER_USER')")
	@ResponseBody
	public APIResponse deleteSearchData() {
		boolean success = true;

		try(Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("delete search_company, search_employee, search_location, search_person from search_person inner join search_employee on search_person.search_person_id = search_employee.search_contact_id inner join search_company on search_employee.search_company_id = search_company.search_company_id inner join search_location on search_location.search_location_id in (search_company.search_location_id, search_person.search_location_id)");
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
			success = false;
		}

		return new GenericAPIResponseBuilder().withSuccess(success).build();
	}

	@PostMapping("/data/index")
	@PreAuthorize("hasRole('ROLE_SUPER_USER')")
	@ResponseBody
	public APIResponse indexSearchData() {
		boolean success = true;

		try {
			new Indexer().indexAll();
		} catch (IOException | IndexerException e) {
			e.printStackTrace();
			//TODO log
			success = false;
		}

		return new GenericAPIResponseBuilder().withSuccess(success).build();
	}

	private static int importSearchData(MultipartFile importFile) throws IOException {
		int dataCount = 0;
		CSVReader csvReader = new CSVReader(new InputStreamReader(importFile.getInputStream()));

		//Factories we need to use
		JobTitleFactory jobTitleFactory = new JobTitleFactory();
		EmployeeFactory employeeFactory = new EmployeeFactory();


		//Objects we'll pre-allocate and re-use
		String[] row;
		Person person;
		JobTitle jobTitle;
		Employee employee;
		Location personLocation;
		Industry industry;
		Company company;
		Location companyLocation;
		Company existingCompany;

		//Column mapper to puts our row data into their correct object fields
		ColumnMapper columnMapper = ColumnMapper.getDefaultMapper();

		//We want to skip over the column headings
		boolean skippedFirstRow = false;

		//Loop through all rows of data
		while((row = csvReader.readNext()) != null) {
			if(skippedFirstRow) {
				person = new Person();
				jobTitle = new JobTitle();
				employee = new Employee();
				employee.setStatus(EmploymentStatus.CURRENT);
				personLocation = new Location();
				industry = new Industry();
				company = new Company();
				companyLocation = new Location();

				columnMapper.mapRowValues(row, person, jobTitle, employee, personLocation, industry, company, companyLocation);

				personLocation.setId(LocationFactory.insertLocation(personLocation));
				person.setLocationId(personLocation.getId());
				person.setId(PersonFactory.insertPerson(person));

				//Try to get an existing company record if it already exists
				existingCompany = CompanyFactory.getCompanyByName(company.getName());
				if (existingCompany != null) {
					company.setId(existingCompany.getId());
					company.setLocationId(existingCompany.getLocationId());
				} else {
					companyLocation.setId(LocationFactory.insertLocation(companyLocation));
					company.setLocationId(companyLocation.getId());
					company.setIndustryId(IndustryFactory.insertOrGetExistingIndustry(industry));
					company.setId(CompanyFactory.insertCompany(company));
				}

				employee.setCompanyId(company.getId());
				employee.setContactId(person.getId());
				employee.setJobTitleId(jobTitleFactory.insertOrGetExistingJobTitle(jobTitle));
				employee.setId(employeeFactory.insertEmployee(employee));

				dataCount++;
			} else {
				skippedFirstRow = true;
			}
		}

		return dataCount;
	}

	private static int importCompanies(MultipartFile importFile) throws IOException {
		int dataCount = 0;
		CSVReader csvReader = new CSVReader(new InputStreamReader(importFile.getInputStream()));

		//Objects we'll pre-allocate and re-use
		String[] row;
		Industry industry;
		Company company;
		Location companyLocation;
		Company existingCompany;

		//Column mapper to puts our row data into their correct object fields
		ColumnMapper columnMapper = ColumnMapper.getCompanyMapper();

		//We want to skip over the column headings
		boolean skippedFirstRow = false;

		//Loop through all rows of data
		while ((row = csvReader.readNext()) != null) {
			if (skippedFirstRow) {
				industry = new Industry();
				company = new Company();
				companyLocation = new Location();

				columnMapper.mapRowValues(row, null, null, null, null, industry, company, companyLocation);

				//Try to get an existing company record if it already exists
				existingCompany = CompanyFactory.getCompanyByName(company.getName());

				if (existingCompany != null) {
					company.setId(existingCompany.getId());
					company.setLocationId(existingCompany.getLocationId());
				} else {
					companyLocation.setId(LocationFactory.insertLocation(companyLocation));
					company.setLocationId(companyLocation.getId());
					company.setIndustryId(IndustryFactory.insertOrGetExistingIndustry(industry));
					company.setId(CompanyFactory.insertCompany(company));
				}

				dataCount++;
			} else {
				skippedFirstRow = true;
			}
		}

		return dataCount;
	}
}
