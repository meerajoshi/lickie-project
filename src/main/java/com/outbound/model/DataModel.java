package com.outbound.model;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class DataModel implements Serializable {

	private LocalDateTime createdOn;
	private LocalDateTime modifiedOn;

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public static void initDataModelFromRS(ResultSet rs, DataModel model) throws SQLException {
		model.setCreatedOn(rs.getObject("created_on", LocalDateTime.class));
		model.setModifiedOn(rs.getObject("last_modified_on", LocalDateTime.class));
	}

}
