package com.outbound.search.employee_search;

public class EmployeeSearch {
	private int id;
	private String firstName;
	private String lastName;
	private String displayName;
	private String function;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	
	public String getDisplayName() {
	    return displayName;
	}
	
	public void setDisplayName(String displayName) {
	    this.displayName = displayName;
	}
	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}
}
