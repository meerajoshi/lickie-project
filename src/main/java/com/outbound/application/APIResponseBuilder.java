package com.outbound.application;

public abstract class APIResponseBuilder {

    private String message;
    private boolean success;

    public String getMessage() {
        return message;
    }

    public APIResponseBuilder withMessage(String message) {
        this.message = message;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public APIResponseBuilder withSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public abstract APIResponse build();

}
