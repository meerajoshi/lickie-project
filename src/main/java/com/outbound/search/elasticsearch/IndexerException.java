package com.outbound.search.elasticsearch;

import org.elasticsearch.action.bulk.BulkResponse;

public class IndexerException extends Exception {
	private BulkResponse response;

	public IndexerException(String message, BulkResponse response) {
		super(message);
		this.response = response;
	}

	public BulkResponse getResponse() {
		return response;
	}

	@Override
	public String getMessage() {
		return response.buildFailureMessage();
	}
}
