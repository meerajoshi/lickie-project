package com.outbound.user;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;
import com.outbound.model.TrackedDataModel;
import org.apache.commons.codec.binary.Base64;
import org.springframework.lang.Nullable;

public class UserInviteFactory extends DBFactory {

	public String insertNewUserInvite(UserInvite userInvite, User user, int createdBy) throws SQLException {
		String userInviteHash = null;

		try (Connection connection = DBConnector.getConnection()) {
			String hash = getInviteHash(user);
			PreparedStatement statement = connection.prepareStatement("insert into user_invite (user_id, invite_hash, completed, created_by) values (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, userInvite.getUserId());
			statement.setString(2, hash);
			statement.setBoolean(3, false);
			statement.setInt(4, createdBy);

			statement.execute();
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()){
				userInviteHash = hash;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return userInviteHash;
	}

	@Nullable
	public UserInvite getUserInvite(String hash, Integer userId) {
		UserInvite invite = null;

		try(Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from user_invite where user_id = ? and invite_hash = ?");
			statement.setInt(1, userId);
			statement.setString(2, hash);

			statement.execute();
			ResultSet rs = statement.getResultSet();

			if(rs.next()){
				invite = initUserInviteFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return invite;
	}

	public List<UserInvite> getUserInvitesForCompany(Integer companyId) throws SQLException {
		List<UserInvite> invites = new ArrayList<>();

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select ui.* from user_invite ui inner join user u on ui.user_id = u.user_id where u.company_id = ?");
			statement.setInt(1, companyId);

			statement.executeQuery();
			ResultSet rs = statement.getResultSet();

			while(rs.next()) {
				invites.add(initUserInviteFromRS(rs));
			}
		}

		return invites;
	}

	public void updateInvite(UserInvite userInvite) {
		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("update user_invite set completed = ? where user_invite_id = ?");
			statement.setBoolean(1, userInvite.isCompleted());
			statement.setInt(2, userInvite.getId());
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}
	}

	private static String getInviteHash(User user) {
		MessageDigest messageDigest = null;

		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			//TODO log
		}

		String stringToHash = user.getEmail() + LocalDateTime.now().toString();

		messageDigest.update(stringToHash.getBytes(StandardCharsets.UTF_8));
		return Base64.encodeBase64URLSafeString(messageDigest.digest());
	}

	public static UserInvite initUserInviteFromRS(ResultSet rs) throws SQLException {
		UserInvite userInvite = new UserInvite();
		userInvite.setId(rs.getInt("user_invite_id"));
		userInvite.setUserId(rs.getInt("user_id"));
		userInvite.setHash(rs.getString("invite_hash"));
		userInvite.setCompleted(rs.getBoolean("completed"));

		TrackedDataModel.initTrackedDataModelFromRS(rs, userInvite);

		return userInvite;
	}

}
