package com.outbound.search.elasticsearch;

public class Mappings {
  public static class Contacts {
    public static final String INDEX_NAME = "contacts";

    public static final String EMPLOYEE_ID_FIELD = "employee_id";
    public static final String CONTACT_ID_FIELD = "contact_id";
    public static final String COMPANY_ID_FIELD = "company_id";
    public static final String COMPANY_FIELD = "company";
    public static final String JOB_TITLE_ID_FIELD = "job_title_id";
    public static final String JOB_TITLE_FIELD = "job_title";
    public static final String SENIORITY_FIELD = "seniority";
    public static final String FUNCTION_FIELD = "function";
    public static final String STATUS_FIELD = "status";
    public static final String NAME_FIELD = "name";
    public static final String EMAIL_FIELD = "email";
    public static final String LINKEDIN_PHOTO = "linkedin_photo";
    public static final String DIRECT_PHONE_FIELD = "direct_phone";
    public static final String CELL_PHONE_FIELD = "cell_phone";
    public static final String LOCATION_FIELD = "location";
  }

  public static class JobTitles {
    public static final String INDEX_NAME = "job_titles";

    public static final String ID_FIELD = "id";
    public static final String JOB_TITLE_FIELD = "job_title";
    public static final String DISPLAY_NAME_FIELD = "display_name";
  }

  public static class Companies {
    public static final String INDEX_NAME = "companies";

    public static final String ID_FIELD = "id";
    public static final String NAME_FIELD = "name";
    public static final String WEBSITE_URL = "websiteUrl";
    public static final String LOGO_URL = "logoUrl";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String PHONE_FIELD = "phone";
    public static final String EMPLOYEE_SIZE_FIELD = "employee_size";
    public static final String INDUSTRY_ID_FIELD = "industry_id";
    public static final String INDUSTRY_FIELD = "industry";
    public static final String LOCATION_FIELD = "location";
  }

  public static class Industries {
    public static final String INDEX_NAME = "industries";

    public static final String ID_FIELD = "id";
    public static final String INDUSTRY_FIELD = "industry";
    public static final String DISPLAY_NAME_FIELD = "display_name";
  }

  public static class Locations {
    public static final String ID_FIELD = "id";
    public static final String CITY_FIELD = "city";
    public static final String STATE_FIELD = "state";
    public static final String COUNTRY_FIELD = "country";
    public static final String POSTAL_FIELD = "postal";
    public static final String ADDRESS_FIELD = "address";
  }
}
