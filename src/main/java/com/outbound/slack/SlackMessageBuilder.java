package com.outbound.slack;

import java.util.ArrayList;
import java.util.List;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;

public class SlackMessageBuilder {

    List<LayoutBlock> blocks = new ArrayList<>();
    List<Attachment> attachments = new ArrayList<>();

    public SlackMessageBuilder addTextBlock(String text) {
        this.blocks.add(SectionBlock.builder().text(PlainTextObject.builder().text(text).build()).build());
        return this;
    }

    public SlackMessageBuilder addMarkdownBlock(String markdown) {
        this.blocks.add(SectionBlock.builder().text(MarkdownTextObject.builder().text(markdown).build()).build());
        return this;
    }

    public SlackMessageBuilder addStringAttachment(String attachmentStr) {
        attachments.add(Attachment.builder().text(attachmentStr).build());
        return this;
    }

    public SlackInteractiveMessage build() {
        return new SlackInteractiveMessage().addBlocks(blocks).addAttachments(attachments);
    }

}
