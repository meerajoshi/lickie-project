package com.outbound.slack;

import java.io.IOException;
import java.util.List;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;

public class SlackWebhooksUtil {

    private static Slack slack = Slack.getInstance();

    public static WebhookResponse send(String webhookUrl, List<LayoutBlock> blocks) throws IOException {
        return slack.send(webhookUrl, Payload.builder().blocks(blocks).build());
    }

    public static WebhookResponse send(String webhookUrl, List<LayoutBlock> blocks, List<Attachment> attachments) throws IOException {
        return slack.send(webhookUrl, Payload.builder().blocks(blocks).attachments(attachments).build());
    }

}
