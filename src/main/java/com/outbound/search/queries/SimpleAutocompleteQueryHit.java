package com.outbound.search.queries;

public class SimpleAutocompleteQueryHit {
  private int id;
  private String displayName;

  public SimpleAutocompleteQueryHit(int id, String displayName) {
    this.id = id;
    this.displayName = displayName;
  }

  public int getId() {
    return id;
  }

  public String getDisplayName() {
    return displayName;
  }
}
