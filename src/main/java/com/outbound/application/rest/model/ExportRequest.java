package com.outbound.application.rest.model;

import java.util.List;

import com.outbound.search.queries.CompaniesQuery;
import com.outbound.search.queries.ContactsQuery;

public class ExportRequest {

	private Integer id;
	private String name;
	private String description;
	private List<Integer> idList;
	private boolean shouldSave;
	private ContactsQuery contactsQuery;
	private CompaniesQuery companiesQuery;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Integer> getIdList() {
		return idList;
	}

	public void setIdList(List<Integer> idList) {
		this.idList = idList;
	}

	public boolean isShouldSave() {
		return shouldSave;
	}

	public void setShouldSave(boolean shouldSave) {
		this.shouldSave = shouldSave;
	}

	public ContactsQuery getContactsQuery() {
		return contactsQuery;
	}

	public void setContactsQuery(ContactsQuery contactsQuery) {
		this.contactsQuery = contactsQuery;
	}

	public CompaniesQuery getCompaniesQuery() {
		return companiesQuery;
	}

	public void setCompaniesQuery(CompaniesQuery companiesQuery) {
		this.companiesQuery = companiesQuery;
	}
}
