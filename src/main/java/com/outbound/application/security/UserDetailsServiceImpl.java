package com.outbound.application.security;

import java.sql.SQLException;
import java.util.List;

import com.outbound.user.UserFactory;
import com.outbound.user.UserRole;
import com.outbound.user.UserRoleFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserFactory userFactory = new UserFactory();
        userFactory.init();

        try {
            com.outbound.user.User user = userFactory.getUser(username);
            String passwordHash = userFactory.getPasswordHash(username);

            if(passwordHash == null) {
                throw new BadCredentialsException("Unable to find username or password.");
            } else {
                UserRoleFactory userRoleFactory = new UserRoleFactory();
                userRoleFactory.init();

                UserRole role = userRoleFactory.getUserRole(user.getUserRoleId());
                List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_" +  role.name());
                return new User(username, passwordHash, grantedAuthorities);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new UsernameNotFoundException("Unable to find username or password.");
        }
    }
}
