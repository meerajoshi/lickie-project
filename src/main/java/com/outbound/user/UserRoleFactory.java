package com.outbound.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;
import org.springframework.lang.Nullable;

public class UserRoleFactory extends DBFactory {

	@Nullable
	public UserRole getUserRole(int roleId) {
		UserRole userRole = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from user_role where user_role_id = ?");
			statement.setInt(1, roleId);

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				userRole = initUserRoleFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return userRole;
	}

	@Nullable
	public UserRole getUserRole(UserRole role) {
		UserRole userRole = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from user_role where name = ?");
			statement.setString(1, role.name());

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				userRole = initUserRoleFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return userRole;
	}

	public List<UserRole> getUserRoles() {
		List<UserRole> roles = new ArrayList<>();

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from user_role");

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				roles.add(initUserRoleFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return roles;
	}

	public static UserRole initUserRoleFromRS(ResultSet rs) throws SQLException {
		String userRoleName = rs.getString("name");
		UserRole userRole = UserRole.valueOf(userRoleName);
		userRole.setRoleId(rs.getInt("user_role_id"));
		return userRole;
	}
}
