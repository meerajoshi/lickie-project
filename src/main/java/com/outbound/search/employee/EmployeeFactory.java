package com.outbound.search.employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class EmployeeFactory extends DBFactory {

	public static Employee getEmployee(Integer employeeId) {
		Employee employee = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_employee where search_employee_id = ?");
			statement.setInt(1, employeeId);

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				employee = initEmployeeFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return employee;
	}

	public static Employee getEmployeeByPersonId(Integer personId) {
		Employee employee = null;

		final String query = "select * from search_employee where search_contact_id = ?";
		try (Connection connection = DBConnector.getConnection(); PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setInt(1, personId);

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				employee = initEmployeeFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return employee;
	}

	/**
	 * WARNING: Only use this for indexing purposes. Not to be used for searching!
	 * @return A list containing all {@link Employee} records in the database.
	 */
	public List<Employee> getAllEmployees() {
		List<Employee> employeeList = new ArrayList<>();

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_employee");

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				employeeList.add(initEmployeeFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return employeeList;
	}

	public List<Employee> getEmployees(List<Integer> ids) {
		List<Employee> employeeList = new ArrayList<>();
		String queryString = String.join(", ", Collections.nCopies(ids.size(), "?"));

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(new StringBuilder("select * from search_employee where search_employee_id in (").append(queryString).append(")").toString());

			for(int idx=1; idx<=ids.size(); idx++) {
				statement.setInt(idx, ids.get(idx-1));
			}

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				employeeList.add(initEmployeeFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return employeeList;
	}
	public Integer insertEmployee(Employee employee) {
		Integer id = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("insert into search_employee (search_contact_id, search_company_id, search_job_title_id, seniority, function, status) values (?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, employee.getContactId());
			statement.setInt(2, employee.getCompanyId());
			statement.setInt(3, employee.getJobTitleId());
			statement.setString(4, employee.getSeniority());
			statement.setString(5, employee.getFunction());
			statement.setString(6, employee.getStatus().name());

			statement.execute();
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return id;
	}

	private static Employee initEmployeeFromRS(ResultSet rs) throws SQLException {
		Employee employee = new Employee();

		employee.setId(rs.getInt("search_employee_id"));
		employee.setContactId(rs.getInt("search_contact_id"));
		employee.setCompanyId(rs.getInt("search_company_id"));
		employee.setJobTitleId(rs.getInt("search_job_title_id"));
		employee.setSeniority(rs.getString("seniority"));
		employee.setFunction(rs.getString("function"));
		employee.setStatus(EmploymentStatus.valueOf(rs.getString("status")));

		return employee;
	}

}
