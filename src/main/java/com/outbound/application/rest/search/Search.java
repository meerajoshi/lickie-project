package com.outbound.application.rest.search;

import java.io.IOException;

import com.outbound.application.APIResponse;
import com.outbound.application.GenericAPIResponseBuilder;
import com.outbound.search.SearchUtil;
import com.outbound.search.elasticsearch.QueryExecutor;
import com.outbound.search.elasticsearch.querybuilders.QueryBuilderUtils;
import com.outbound.search.elasticsearch.querybuilders.SimpleAutocompleteQueryBuilder;
import com.outbound.search.queries.CompaniesQuery;
import com.outbound.search.queries.CompaniesQueryHit;
import com.outbound.search.queries.ContactsQuery;
import com.outbound.search.queries.ContactsQueryHit;
import com.outbound.search.queries.QueryResponse;
import com.outbound.search.queries.SimpleAutocompleteQuery;
import com.outbound.search.queries.SimpleAutocompleteQueryHit;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/search")
@PreAuthorize("isAuthenticated()")
public class Search {

  @PostMapping(
    value = "/contacts",
    consumes = { MediaType.APPLICATION_JSON_VALUE },
    produces = { MediaType.APPLICATION_JSON_VALUE }
  )
  public APIResponse searchContacts(@RequestBody ContactsQuery query) throws IOException {
    GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();
    QueryResponse<ContactsQueryHit> response = SearchUtil.doContactsSearch(query);
    return responseBuilder.withData(response).withSuccess(true).build();
  }

  @PostMapping(
    value = "/companies",
    consumes = { MediaType.APPLICATION_JSON_VALUE },
    produces = { MediaType.APPLICATION_JSON_VALUE }
  )
  public APIResponse searchCompanies(@RequestBody CompaniesQuery query) throws IOException {
    GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();
    QueryResponse<CompaniesQueryHit> response = SearchUtil.doCompaniesSearch(query);
    return responseBuilder.withData(response).withSuccess(true).build();
  }

  @PostMapping(
    value = "/jobTitles",
    consumes = { MediaType.APPLICATION_JSON_VALUE },
    produces = { MediaType.APPLICATION_JSON_VALUE }
  )
  public APIResponse searchJobTitles(@RequestBody SimpleAutocompleteQuery query) throws IOException {
    GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();

    SimpleAutocompleteQueryBuilder queryBuilder = QueryBuilderUtils.createJobTitleQueryBuilder(query);
    QueryResponse<SimpleAutocompleteQueryHit> response = new QueryExecutor<>(queryBuilder).execute();

    return responseBuilder.withData(response).withSuccess(true).build();
  }

  @PostMapping(
    value = "/industries",
    consumes = { MediaType.APPLICATION_JSON_VALUE },
    produces = { MediaType.APPLICATION_JSON_VALUE }
  )
  public APIResponse searchIndustries(@RequestBody SimpleAutocompleteQuery query) throws IOException {
    GenericAPIResponseBuilder responseBuilder = new GenericAPIResponseBuilder();

    SimpleAutocompleteQueryBuilder queryBuilder = QueryBuilderUtils.createIndustryQueryBuilder(query);
    QueryResponse<SimpleAutocompleteQueryHit> response = new QueryExecutor<>(queryBuilder).execute();

    return responseBuilder.withData(response).withSuccess(true).build();
  }
}
