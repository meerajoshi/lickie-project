package com.outbound.search.queries;

import java.util.List;

public class QueryResponse<T> {
  private long totalHits;
  private long offset;
  private List<T> hits;

  public QueryResponse(long totalHits, long offset, List<T> hits) {
    this.totalHits = totalHits;
    this.offset = offset;
    this.hits = hits;
  }

  public long getOffset() {
    return offset;
  }

  public long getTotalHits() {
    return totalHits;
  }

  public List<T> getHits() {
    return hits;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("QueryResponse(totalHits=");
    stringBuilder.append(this.totalHits);
    stringBuilder.append(", hits=[");

    for (T hit : this.hits) {
      stringBuilder.append(hit.toString());
      stringBuilder.append(",");
    }

    stringBuilder.append("])");

    return stringBuilder.toString();
  }
}
