package com.outbound.search.person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.outbound.database.DBConnector;
import com.outbound.database.DBFactory;

public class PersonFactory extends DBFactory {

	public static Person getPerson(Integer id) {
		Person person = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_person where search_person_id = ?");
			statement.setInt(1, id);

			ResultSet rs = statement.executeQuery();

			if(rs.next()) {
				person = initPersonFromRS(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return person;
	}

	/**
	 * WARNING: Only use this for indexing purposes. Not to be used for searching!
	 * @return A list containing all {@link Person} records in the database.
	 */
	public static List<Person> getAllPersons() {
		List<Person> personList = new ArrayList<>();

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("select * from search_person");

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				personList.add(initPersonFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return personList;
	}

	public static List<Person> getPersons(List<Integer> ids) {
		List<Person> personList = new ArrayList<>();
		String queryString = String.join(", ", Collections.nCopies(ids.size(), "?"));

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(new StringBuilder("select * from search_person where search_person_id in (").append(queryString).append(")").toString());

			for(int idx=1; idx<=ids.size(); idx++) {
				statement.setInt(idx, ids.get(idx-1));
			}

			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				personList.add(initPersonFromRS(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return personList;
	}

	public static Integer insertPerson(Person person) {
		Integer id = null;

		try (Connection connection = DBConnector.getConnection()) {
			PreparedStatement statement = connection.prepareStatement("insert into search_person (first_name, last_name, search_location_id, email, direct_phone, cell_phone, linkedin_profile, linkedin_photo) values (?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, person.getFirstName());
			statement.setString(2, person.getLastName());
			statement.setInt(3, person.getLocationId());
			statement.setString(4, person.getEmail());
			statement.setString(5, person.getDirectPhone());
			statement.setString(6, person.getCellPhone());
			statement.setString(7, person.getLinkedinProfile());
			statement.setString(8, person.getLinkedinPhoto());

			statement.execute();
			ResultSet rs = statement.getGeneratedKeys();
			if(rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//TODO log
		}

		return id;
	}

	private static Person initPersonFromRS(ResultSet rs) throws SQLException {
		Person person = new Person();

		person.setId(rs.getInt("search_person_id"));
		person.setFirstName(rs.getString("first_name"));
		person.setLastName(rs.getString("last_name"));
		person.setLocationId(rs.getInt("search_location_id"));
		person.setEmail(rs.getString("email"));
		person.setDirectPhone(rs.getString("direct_phone"));
		person.setCellPhone(rs.getString("cell_phone"));
		person.setLinkedinProfile(rs.getString("linkedin_profile"));
		person.setLinkedinPhoto(rs.getString("linkedin_photo"));

		return person;
	}

}
