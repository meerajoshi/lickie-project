package com.outbound.application.rest.employee;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.outbound.application.APIResponse;
import com.outbound.application.GenericAPIResponseBuilder;
import com.outbound.application.rest.model.EmployeeDetails;
import com.outbound.application.rest.model.EmployeeDetailsUtil;

@RestController
@RequestMapping("/employees")
@PreAuthorize("isAuthenticated()")
public class SearchByCompanyName {

	@GetMapping("/get")
	@ResponseBody
	public APIResponse getEmployees(@RequestParam(name = "companyId") Integer id) {
		EmployeeDetails companyDetails = EmployeeDetailsUtil.getCurrentCompanyEmployee(id);
		return new GenericAPIResponseBuilder().withData(companyDetails).build();
	}
}
